import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

// Instalacion Components
import { Step01Component } from './components/instalacion/step01/step01.component';
import { Step02Component } from './components/instalacion/step02/step02.component';
import { SyncComponent } from './components/sync/sync.component';

import { CompPruebaComponent } from './components/instalacion/comp-prueba/comp-prueba.component';
//Ruta
import { RutaComponent } from './components/ruta/ruta.component';

// Cobradores Components
import { MainCobradoresComponent } from './components/cobradores/main-cobradores/main-cobradores.component';
import { CobranzasListComponent } from './components/cobradores/cobranzas-list/cobranzas-list.component';
import { OrdenesListComponent } from './components/cobradores/ordenes-list/ordenes-list.component';
import { OrdenesDetalleComponent } from './components/cobradores/ordenes-detalle/ordenes-detalle.component';
import { ClienteMapComponent } from './components/cobradores/cliente-map/cliente-map.component';
import { CobAgendaFormComponent } from './components/cobradores/cob-agenda-form/cob-agenda-form.component';
import { CobAgendaListComponent } from './components/cobradores/cob-agenda-list/cob-agenda-list.component';
import { CobAgendaDetalleComponent } from './components/cobradores/cob-agenda-detalle/cob-agenda-detalle.component';
import { PendientesMapComponent } from './components/cobradores/pendientes-map/pendientes-map.component';
import { RendicionDetalleComponent } from './components/cobradores/rendicion-detalle/rendicion-detalle.component';
import { RendicionesListComponent } from './components/cobradores/rendiciones-list/rendiciones-list.component';

// Productores Components
import { MainProductoresComponent } from './components/productores/main-productores/main-productores.component';
import { AgendaListComponent } from './components/productores/agenda-list/agenda-list.component';
import { AgendaAddFormComponent } from './components/productores/agenda-add-form/agenda-add-form.component';
import { AgendaDetalleComponent } from './components/productores/agenda-detalle/agenda-detalle.component';
import { AgendaModificarComponent } from './components/productores/agenda-modificar/agenda-modificar.component';
import { AgendaMapComponent } from './components/productores/agenda-map/agenda-map.component';

// Helper Directives
import { LoadingDarkOverlayComponent } from './helper-directives/loading-dark-overlay/loading-dark-overlay.component';

import { CobdataService } from './services/cobdata.service';

import { DateFormatPipe } from './custom-pipes/appdate-pipe';
import { AppCurrencyPipe } from './custom-pipes/appcurrency-pipe';
import { DateFormatdmPipe } from './custom-pipes/appdatedm-pipe';

// Plugins
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DateFormatPipe,
    AppCurrencyPipe,
    DateFormatdmPipe,
    AgendaListComponent,
    AgendaAddFormComponent,
    MainProductoresComponent,
    AgendaDetalleComponent,
    AgendaModificarComponent,
    MainCobradoresComponent,
    CobranzasListComponent,
    OrdenesListComponent,
    OrdenesDetalleComponent,
    ClienteMapComponent,
    CobAgendaFormComponent,
    CobAgendaListComponent,
    CobAgendaDetalleComponent,
    PendientesMapComponent,
    AgendaMapComponent,
    LoadingDarkOverlayComponent,
    RendicionDetalleComponent,
    RendicionesListComponent,
    Step01Component,
    Step02Component,
    CompPruebaComponent,
    SyncComponent,
    RutaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OnsenModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    NgxMyDatePickerModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBw0XJ6fHBD1tOYByyVDRoEeo8bqSvVqwQ'
    })
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [CobdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
