export interface Session {
    loggedUserId?: number;
    licenciaCliente?: string;
    cobradorId?: number;
}
