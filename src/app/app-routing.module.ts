import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';

// Install Components
import { Step01Component } from './components/instalacion/step01/step01.component';
import { Step02Component } from './components/instalacion/step02/step02.component';
//sync
import { SyncComponent } from './components/sync/sync.component';

//ruta
import { RutaComponent } from './components/ruta/ruta.component';

// Cobradores Components
import { MainCobradoresComponent } from './components/cobradores/main-cobradores/main-cobradores.component';
import { CobranzasListComponent } from './components/cobradores/cobranzas-list/cobranzas-list.component';
import { OrdenesListComponent } from './components/cobradores/ordenes-list/ordenes-list.component';
import { OrdenesDetalleComponent } from './components/cobradores/ordenes-detalle/ordenes-detalle.component';
import { ClienteMapComponent } from './components/cobradores/cliente-map/cliente-map.component';
import { CobAgendaFormComponent } from './components/cobradores/cob-agenda-form/cob-agenda-form.component';
import { CobAgendaListComponent } from './components/cobradores/cob-agenda-list/cob-agenda-list.component';
import { CobAgendaDetalleComponent } from './components/cobradores/cob-agenda-detalle/cob-agenda-detalle.component';
import { PendientesMapComponent } from './components/cobradores/pendientes-map/pendientes-map.component';
import { RendicionDetalleComponent } from './components/cobradores/rendicion-detalle/rendicion-detalle.component';
import { RendicionesListComponent } from './components/cobradores/rendiciones-list/rendiciones-list.component';

// Productores Componets
import { MainProductoresComponent } from './components/productores/main-productores/main-productores.component';
import { AgendaListComponent } from './components/productores/agenda-list/agenda-list.component';
import { AgendaAddFormComponent } from './components/productores/agenda-add-form/agenda-add-form.component';
import { AgendaDetalleComponent } from './components/productores/agenda-detalle/agenda-detalle.component';
import { AgendaModificarComponent } from './components/productores/agenda-modificar/agenda-modificar.component';
import { AgendaMapComponent } from './components/productores/agenda-map/agenda-map.component';

const routes: Routes = [

  // Login Route - Default route
  { path: '', component: LoginComponent },

  // Instalacion Routes

  { path: 'step-01', component: Step01Component },
  { path: 'step-02', component: Step02Component },
  { path: 'Sync', component: SyncComponent },

  //ruta
  { path: 'Ruta', component: RutaComponent },

  // Cobradores Routes
  { path: 'mainc', component: MainCobradoresComponent },
  { path: 'cobranzasList', component: CobranzasListComponent },
  { path: 'ordenesList', component: OrdenesListComponent },
  { path: 'ordenesDetalle', component: OrdenesDetalleComponent },
  { path: 'clienteMap', component: ClienteMapComponent },
  { path: 'formulario-agenda-cobrador', component: CobAgendaFormComponent },
  { path: 'listado-agenda-cobrador', component: CobAgendaListComponent },
  { path: 'detalle-agenda-cobrador', component: CobAgendaDetalleComponent },
  { path: 'domicilios-pendientes-map', component: PendientesMapComponent },
  { path: 'detalle-rendicion', component: RendicionDetalleComponent },
  { path: 'listado-rendiciones', component: RendicionesListComponent },

  // Productores Routes
  { path: 'mainp', component: MainProductoresComponent },
  { path: 'agenda', component: AgendaListComponent },
  { path: 'agendaForm', component: AgendaAddFormComponent },
  { path: 'agendaDetalle', component: AgendaDetalleComponent },
  { path: 'agendaModificar', component: AgendaModificarComponent },
  { path: 'agenda-map', component: AgendaMapComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
