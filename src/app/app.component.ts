import {
  Component,
  OnsenModule,
  NgModule,
  ViewChild,
  Injectable,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as ons from 'onsenui';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/observable/timer'
import { CobdataService } from './services/cobdata.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
	name = 'Angular 5';
	  alive = true;

	  constructor(private cobData: CobdataService,) {
	    Observable.timer(0,10000)
	    .takeWhile(() => this.alive) // only fires when component is alive
	    .subscribe(() => {
	      this.cobData.getGpsTxt().subscribe(data=> {
	      	if (navigator.geolocation){
          
		        navigator.geolocation.getCurrentPosition((position) => {
			        const lat = position.coords.latitude.toString();
		        	const lon = position.coords.longitude.toString();
			        this.cobData.latitud=position.coords.latitude;
			        this.cobData.longitud= position.coords.longitude;
			        this.cobData.ActualizarGpsRuta(lat,lon).subscribe(data=> {        
			        })
		      	})

		      }else{
		      }

	       /* const gpsTxt = data;
	        const commaIndex = gpsTxt.indexOf(',');
	        const lat = gpsTxt.slice(0, commaIndex);
	        const lon = gpsTxt.slice(commaIndex + 1);
	        this.cobData.latitud=lat;
	        this.cobData.longitud=lon;
	        this.cobData.ActualizarGpsRuta(lat,lon).subscribe(data=> {
		        
		        
		      })*/

	      })
	    });
	  }

	  ngOnDestroy(){
    this.alive = false; // switches your IntervalObservable off
  }

}
