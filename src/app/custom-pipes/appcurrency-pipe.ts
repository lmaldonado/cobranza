import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
    name: 'appCurrency'
})
export class AppCurrencyPipe implements PipeTransform {

    transform(value: any,
        currencySign: string = '$',
        decimalLength: number = 2,
        chunkDelimiter: string = '.',
        decimalDelimiter: string = ',',
        chunkLength: number = 3): string {

        if (typeof(value) === typeof('string')) {
            value = parseFloat(value);
        }

        const result = '\\d(?=(\\d{' + chunkLength + '})+' + (decimalLength > 0 ? '\\D' : '$') + ')';
        const num = value.toFixed(Math.max(0, decimalLength));

        return currencySign + (decimalDelimiter ? num.replace('.', decimalDelimiter) : num)
        .replace(new RegExp(result, 'g'), '$&' + chunkDelimiter);
    }

}