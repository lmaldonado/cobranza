import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, DoCheck, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { LoadingDarkOverlayComponent } from '../../../helper-directives/loading-dark-overlay/loading-dark-overlay.component';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-agenda-modificar',
  templateUrl: './agenda-modificar.component.html',
  styleUrls: ['./agenda-modificar.component.scss']
})
export class AgendaModificarComponent implements OnInit, DoCheck, OnDestroy {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/agendaModificar';
  homeComponentRoute = '/mainp';
  menuCollapsed = true;
  cliente: any;
  loading = false;
  formNombre: string;
  formTelefono: string;
  formDireccion: string;
  formFecha: any;
  formFechaInit: any;
  formTiempo = '';
  formHora: string;
  formMinuto: string;
  formFechaFormatted: string;
  formEstado: string;
  formObservaciones: string;
  formDni: string;
  formFechaNacimiento: any;
  formFechaNacimientoFormatted: any;
  clienteAlta = false;
  fechaPlaceholder: string;
  formNumeroContrato: string;
  formImporte: string;
  //variable para pasar segun estado
  Dni:string;
  FechaN:any;
  Contrato: string;
  Importe: string;
  fechaV:string;

  // Param Properties
  paramNombre: string;
  paramTelefono: string;
  paramDireccion: string;
  paramFechaHora: string;
  paramObservaciones: string;
  paramFechaNacimiento: string;
  paramImporte: string;
  paramLatitud: string;
  paramLongitud: string;

  // fecha
  dia: string;
  mes: string;
  ano: string;

  // gps
  gpsTxtLatitud: number;
  gpsTxtLongitud: number;

  myDatePickerOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    dayLabels: { su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mie', th: 'Jue', fr: 'Vie', sa: 'Sab' },
    monthLabels: {
      1: 'Ene', 2: 'Feb', 3: 'Mar', 4: 'Abr', 5: 'May', 6: 'Jun',
      7: 'Jul', 8: 'Ago', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dic'
    },
    todayBtnTxt: 'Hoy'
  };

  ngOnInit() {

    this.cliente = this.cobData.clienteSeleccionado;
    // console.log(this.cliente);
    this.clienteAlta = this.cliente.IdEstatusVisita === '2' ? true : false;

    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
    if (this.cobData.tipoUsuario !== 'productor') {
      this.router.navigate(['']);
    }
    if (!this.cliente) {
      this.router.navigate(['']);
    }

    console.log(this.cobData.aRoute);

    this.formNombre = this.cliente.NombreCliente;
    this.formTelefono = this.cliente.Telefono;
    this.formDireccion = this.cliente.Direccion;
    this.formHora = this.cliente.Hora ? this.cliente.Hora : '0';
    this.formMinuto = this.cliente.Minutos ? this.cliente.Minutos : '0';
    this.formFechaInit = `${this.cliente.Dia}-${this.cliente.Mes}-${this.cliente.Ano}`;
    this.formTiempo = `${this.formHora}:${this.formMinuto}`;
    this.formFechaFormatted = `${this.formFechaInit} ${this.formHora}:${this.formMinuto}:00`;
    this.paramLatitud = this.cliente.LatitudCliente;
    this.paramLongitud = this.cliente.LongitudCliente;

    if (this.cliente.FechaVisita) {
      this.formFecha = { date: { year: this.cliente.Ano, month: this.cliente.Mes, day: this.cliente.Dia } };
    } else {
      this.fechaPlaceholder = `Fecha de visita...`;
    }

    if (this.cliente.FechaNacimiento) {
      this.formFechaNacimiento = { date: { year: this.cliente.Anon, month: this.cliente.Mesn, day: this.cliente.Dian } };
    }

    this.formObservaciones = this.cliente.Observacion;
    // this.formEstado = this.cliente.IdEstatusVisita;
    this.formEstado = this.cobData.estadoInicial ? this.cobData.estadoInicial.toString() : this.cliente.IdEstatusVisita;
    this.formDni = this.cliente.DniTitular;
    this.formNumeroContrato = this.cliente.NumeroContrato;
    this.formImporte = this.cliente.ImportePlan;

    // console.log(this.cliente);

  }

  ngOnDestroy() {
    this.cobData.estadoInicial = undefined;
  }

  ngDoCheck() {
    this.initiateMasks();
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  getCurrentLocation(): Promise<any> {
    let gpsTxt: string;
      const q = new Promise((resolve, reject) => {
      this.cobData.getGpsTxt().subscribe((data) => {
        gpsTxt = data;
        const commaIndex = gpsTxt.indexOf(',');
        const lat = gpsTxt.slice(0, commaIndex);
        const lon = gpsTxt.slice(commaIndex + 1);
        this.gpsTxtLatitud = parseFloat(lat);
        this.gpsTxtLongitud = parseFloat(lon);
        resolve({ gotCurrentLocation: true});
      });
    });
    return q;
  }

  actualizarGPS() {
    const msg = `Esta seguro que desea actualizar la ubicación del cliente ${this.cliente.NombreCliente} con su ubicación actual?`;
    const confirmMsg = `La ubicación del cliente ${this.cliente.NombreCliente} se ha actualizado con exito!`;
    ons.notification.confirm(msg, { title: '', buttonLabels: ['No', 'Si'] })
    .then((res) => {
      if (res) {
        this.loading = true;
        this.getCurrentLocation().then((resolvedData) => {
          const lat = this.gpsTxtLatitud.toString();
          const lon = this.gpsTxtLongitud.toString();
          this.cobData.putActualizarGps(1, this.cliente.IdRegistro, lat, lon)
          .subscribe((data) => {
            this.cobData.SyncCobradorPol(this.cobData.cobradorId).subscribe((data)=>{

            });
            const updated = data[0];
            this.cliente.LatitudCliente = updated.LatitudCliente;
            this.cliente.LongitudCliente = updated.LongitudCliente;
            ons.notification.alert(confirmMsg, { title: '' });
            this.loading = false;
          });
        });
      }
    });
    this.loading = false;
  }

  initiateMasks() {
    let charLen: string;

    charLen = ''; for (let i = 0; i <= 25; i++) { charLen = charLen + 'S'; }
    $('#formNombre').mask(charLen, {placeholder: 'Nombre...', 'translation': {S: {pattern: /[^,;/\\\\*%&()'"]+/}}});

    charLen = ''; for (let i = 0; i <= 15; i++) { charLen = charLen + 'S'; }
    $('#formDni').mask(charLen, {placeholder: 'DNI...', 'translation': {S: {pattern: /[^,;/\\\\*%&()'"]+/}}});

    charLen = ''; for (let i = 0; i <= 15; i++) { charLen = charLen + 'S'; }
    $('#formNumeroContrato').mask(charLen, {placeholder: 'Contrato...', 'translation': {S: {pattern: /[^,;/\\\\*%&()'"]+/}}});

    $('#formdp').mask('00-00-0000', {placeholder: '__-__-____'});
    $('#formFechaNacimiento').mask('00-00-0000', {placeholder: '__-__-____'});
    $('#formTelefono').mask('000-0000-0000000000', {placeholder: '000-0000-0000'});
    $('#formTiempo').mask('H0:M0', {placeholder: 'HH:MM', 'translation': {H: {pattern: /^[0-2]$/}, M: {pattern: /^[0-5]$/}}});

    const translationObject = {
      placeholder: '0000,00',
      'translation': {
        0: {pattern: /^[0-9]+$/},
        S: {pattern: /^[0-9,]+$/}
      }
    };
    $('#formImporte').mask('0SSSSSSSSSS0', translationObject);
  }

  onDateChanged(dateEvent: IMyDateModel): void {
      this.formFecha = dateEvent.formatted;
      this.formFechaFormatted = `${dateEvent.formatted} ${this.formHora}:${this.formMinuto}:00`;
  }

  onDateChangedNacimiento(dateEvent: IMyDateModel): void {
    this.formFechaNacimientoFormatted = `${dateEvent.formatted}`;
    
  }

  onTimeChanged() {
    const fecha = (this.formFechaFormatted) ? this.formFechaInit : this.formFechaFormatted;
    this.formFechaFormatted = `${fecha} ${this.formHora}:${this.formMinuto}:00`;
  }

  validateFormORIGINAL() {

    if (!this.formNombre) {
      ons.notification.alert('Debe Ingresar un nombre de cliente', {title: ''});
      return false;
    }

    if (!this.formTelefono) {
      ons.notification.alert('Debe Ingresar un numero de telefono', {title: ''});
      return false;
    }

    if (!this.formDireccion) {
      ons.notification.alert('Debe Ingresar una direccion', {title: ''});
      return false;
    }

    if (!this.formFecha && (this.formEstado === '1' || this.formEstado === '4')) {
      ons.notification.alert('Debe Ingresar una fecha de visita', {title: ''});
      return false;
    }

    if ((this.formHora === '0' || this.formMinuto === '0') && (this.formEstado === '1' || this.formEstado === '4')) {
      ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
      return false;
    }

    if (!this.formDni && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar el DNI del cliente.', {title: ''});
      return false;
    }

    if (!this.formNumeroContrato && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar el número de contrato.', {title: ''});
      return false;
    }

    if (!this.formImporte && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar importe del contrato.', {title: ''});
      return false;
    }

    if (!this.formFechaNacimiento && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar la fecha de nacimiento del cliente', {title: ''});
      return false;
    }

    if (this.formFecha) {
      this.dia = this.formFecha.date.day >= 10 ? `${this.formFecha.date.day}` : `0${this.formFecha.date.day}`;
      this.mes = this.formFecha.date.month >= 10 ? `${this.formFecha.date.month}` : `0${this.formFecha.date.month}`;
      this.ano = this.formFecha.date.year.toString();
      this.paramFechaHora = `${this.ano}-${this.mes}-${this.dia} ${this.formHora}:${this.formMinuto}:00`;
    }

    if (this.formEstado === '2') {
      this.paramFechaHora = undefined;
    }

    if (this.formFechaNacimiento) {
      this.dia = this.formFechaNacimiento.date.day >= 10 ? `${this.formFechaNacimiento.date.day}` : `0${this.formFechaNacimiento.date.day}`;
      this.mes = this.formFechaNacimiento.date.month >= 10 ? `${this.formFechaNacimiento.date.month}` : `0${this.formFechaNacimiento.date.month}`;
      this.ano = this.formFechaNacimiento.date.year.toString();
      this.paramFechaNacimiento = `${this.ano}-${this.mes}-${this.dia}`;
    }

    return true;
  }

  validateForm() {

    const REG_EX = /[,;/\\\\*%&()'"]+/g;

    if (!this.formNombre) {
      ons.notification.alert('Debe Ingresar un nombre de cliente', {title: ''});
      return false;
    }

    if (!this.formTelefono) {
      this.formTelefono = null;
    }

    if (!this.formDireccion && this.formEstado !== '3') {
      ons.notification.alert('Debe Ingresar una dirección', {title: ''});
      return false;
    }

    if (!this.formFecha && (this.formEstado === '1' || this.formEstado === '4')) {
      ons.notification.alert('Debe Ingresar una fecha de visita valida', {title: ''});
      return false;
    }

    /*if ((this.formHora === '0' || this.formMinuto === '0') && (this.formEstado === '1' || this.formEstado === '4')) {
      ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
      return false;
    }*/

    if (this.formTiempo.length !== 5 && (this.formEstado === '1' || this.formEstado === '4')) {
      ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
      this.formTiempo = '';
      return false;
    } else if (this.formTiempo.length === 5 && (this.formEstado === '1' || this.formEstado === '4')) {
      const h = parseFloat(this.formTiempo.substr(0, 2));
      const m = parseFloat(this.formTiempo.substr(3, 4));
      if (h > 23 || m > 59) {
        ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
        this.formTiempo = '';
        return false;
      } else {
        this.formHora = this.formTiempo.substr(0, 2);
        this.formMinuto = this.formTiempo.substr(3, 4);
      }
    }

    if (!this.formObservaciones) {
      this.formObservaciones = null;
    } else {
      this.paramObservaciones = this.formObservaciones.replace(REG_EX, ' ');
    }

    if (!this.formDni && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar el DNI del cliente.', {title: ''});
      return false;
    }

    if (!this.formNumeroContrato && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar el número de contrato.', {title: ''});
      return false;
    }

    if (!this.formImporte && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar importe del contrato.', {title: ''});
      return false;
    } else if (this.formImporte && this.formEstado === '2') {
      let countSeparators = 0;
      for (let i = 0; i < this.formImporte.length; i++) {
        if (this.formImporte.charAt(i) === ',') { countSeparators++; }
      }
      if (countSeparators > 1) {
        ons.notification.alert('El importe debe ser de la forma 0000,00', {title: ''});
        return false;
      }
      if (this.formImporte.charAt(this.formImporte.length - 1) === ',') {
        ons.notification.alert('El importe debe ser de la forma 0000,00', {title: ''});
        return false;
      }
      this.paramImporte = this.formImporte.replace(',', '.');
    }

    if (!this.formFechaNacimiento && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar una fecha de nacimiento valida', {title: ''});
      return false;
    }

    this.paramNombre = this.formNombre;
    this.paramTelefono = this.formTelefono;
    this.paramDireccion = this.formDireccion.replace(REG_EX, ' ');

    if (this.formFecha) {
      this.dia = this.formFecha.date.day >= 10 ? `${this.formFecha.date.day}` : `0${this.formFecha.date.day}`;
      this.mes = this.formFecha.date.month >= 10 ? `${this.formFecha.date.month}` : `0${this.formFecha.date.month}`;
      this.ano = this.formFecha.date.year.toString();
      this.paramFechaHora = `${this.ano}-${this.mes}-${this.dia} ${this.formHora}:${this.formMinuto}:00`;
    }

    if (this.formFechaNacimiento) {
      this.dia = this.formFechaNacimiento.date.day >= 10 ? `${this.formFechaNacimiento.date.day}` : `0${this.formFechaNacimiento.date.day}`;
      this.mes = this.formFechaNacimiento.date.month >= 10 ? `${this.formFechaNacimiento.date.month}` : `0${this.formFechaNacimiento.date.month}`;
      this.ano = this.formFechaNacimiento.date.year.toString();
      this.paramFechaNacimiento = `${this.ano}-${this.mes}-${this.dia}`;
      console.log(this.paramFechaNacimiento);
    }

    return true;
  }

  /* --------- Metodos Ruteables ------------------ */
  modificarCliente() {
    this.loading = true;
      this.Dni=null; 
      this.FechaN=null;
      this.Contrato=null;
      this.Importe=null;
      
    if (this.validateForm()) {
      this.fechaV = this.paramFechaHora;
      if (this.formEstado === '2') {
         this.Dni=this.formDni;
         this.FechaN=this.paramFechaNacimiento;
         this.Contrato=this.formNumeroContrato;
         this.Importe= this.paramImporte;
         this.fechaV= null;
        }
      const paramsArray = [{
        idRegistro: this.cliente.IdRegistro,
        idEstatusVisita: this.formEstado.toString(),
        nombreCliente: this.paramNombre,
        telefono: this.paramTelefono,
        direccion: this.paramDireccion,
        fechaVisita: this.fechaV,
        observacion: this.paramObservaciones,
        latitud: this.paramLatitud,
        longitud: this.paramLongitud,
        dni: this.Dni,
        contrato: this.Contrato,
        importe: this.Importe,
        fechaNacimiento: this.FechaN
      }];
       console.log(paramsArray);
      this.cobData.putModificarCliente(paramsArray)
          .subscribe( (data) => {
            ons.notification.alert('Datos del cliente modificados!', { title: '' });
            this.loading = false;
            this.goBack();
          });
    } else {
      this.loading = false;
    }
  }
  /* ---------------------------------------------- */

}
