import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, DoCheck, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { LoadingDarkOverlayComponent } from '../../../helper-directives/loading-dark-overlay/loading-dark-overlay.component';

@Component({
  selector: 'app-agenda-detalle',
  templateUrl: './agenda-detalle.component.html',
  styleUrls: ['./agenda-detalle.component.scss']
})
export class AgendaDetalleComponent implements OnInit, OnDestroy {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/agendaDetalle';
  homeComponentRoute = '/mainp';
  menuCollapsed = true;
  cliente: any;
  gpsTxt: any;
  gpsTxtLatitud: number;
  gpsTxtLongitud: number;
  gotCurrentLocation = false;
  loading = false;
  idRegistro: number;
  isAlta = false;

  ngOnInit() {

    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
    if (this.cobData.tipoUsuario !== 'productor') {
      this.router.navigate(['']);
    }

    // console.log(this.cobData.aRoute);
    // this.cliente = this.cobData.clienteSeleccionado;
    // console.log(this.cliente);
    this.idRegistro = this.cobData.IdRegistroAgenda;
    this.cobData.getClientesAgendaDetalle(this.idRegistro).subscribe((data) => {
      // console.log(data[0]);
      this.cliente = data[0];
    });
    this.getCurrentLocation();

  }

  ngOnDestroy() {
    this.cliente = undefined;
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  getCurrentLocation() {
    this.cobData.getGpsTxt().subscribe((data) => {
      this.gpsTxt = data;
      const commaIndex = this.gpsTxt.indexOf(',');
      const lat = this.gpsTxt.slice(0, commaIndex);
      const lon = this.gpsTxt.slice(commaIndex + 1);
      this.gpsTxtLatitud = parseFloat(lat);
      this.gpsTxtLongitud = parseFloat(lon);
      this.gotCurrentLocation = true;
    });
  }

  actualizarGPS() {
    const msg = `Esta seguro que desea actualizar la ubicación del cliente ${this.cliente.NombreCliente} con su ubicación actual?`;
    const confirmMsg = `La ubicación del cliente ${this.cliente.NombreCliente} se ha actualizado con exito!`;
    ons.notification.confirm(msg, { title: '', buttonLabels: ['No', 'Si'] })
    .then((res) => {
      if (res) {
        this.loading = true;
        const lat = this.gpsTxtLatitud.toString();
        const lon = this.gpsTxtLongitud.toString();
        this.cobData.putActualizarGps(1, this.cliente.IdRegistro, lat, lon)
        .subscribe((data) => {
          const updated = data[0];
          this.cobData.SyncCobradorPol(this.cobData.cobradorId).subscribe((data)=>{

          });
          this.cliente.LatitudCliente = updated.LatitudCliente;
          this.cliente.LongitudCliente = updated.LongitudCliente;
          ons.notification.alert(confirmMsg, { title: '' });
          this.loading = false;
        });
      }
    });
    this.loading = false;
  }

  /* --------- Metodos Ruteables ------------------ */
  seeMap() {
    const gpsObject = {
      lat: parseFloat(this.cliente.LatitudCliente),
      lon: parseFloat(this.cliente.LongitudCliente),
      diasDiferencia: parseInt(this.cliente.DiferenciaFecha, 10),
      cliente: this.cliente.NombreCliente,
      telefono: this.cliente.Telefono,
      direccion: this.cliente.Direccion
    };
    this.cobData.gpsAgendados = gpsObject;
    this.cobData.flagClienteMap = true;
    this.goTo('agenda-map');
  }

  deleteCliente() {
    const msg = `Esta seguro que desea eliminar de la agenda al cliente ${this.cliente.NombreCliente}?`;
    ons.notification.confirm(msg, {title: 'Confirmación', buttonLabels: ['Cancelar', 'Eliminar']})
    .then((res) => {
      if (res) {
        this.loading = true;
        this.cobData.deleteClienteAgenda(this.cliente.IdRegistro).toPromise()
        .then((data) => {
          ons.notification.alert(`Cliente ${this.cliente.NombreCliente} Eliminado`, { title: 'Notificación' });
          this.cliente = '';
          this.loading = false;
          this.router.navigate(['agenda']);
        });
      } else {
        this.loading = false;
      }
    });
  }

  darAltaCliente() {
    this.cobData.clienteSeleccionado = this.cliente;
    this.cobData.estadoInicial = 2;
    this.goTo('agendaModificar');
  }

  modificarCliente() {
    this.cobData.clienteSeleccionado = this.cliente;
    this.cobData.cameFromModificar = true;
    this.goTo('agendaModificar');
  }
  /* ---------------------------------------------- */

}
