import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-main-productores',
  templateUrl: './main-productores.component.html',
  styleUrls: ['./main-productores.component.scss']
})
export class MainProductoresComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/mainp';
  appLogo = '../../../../assets/appLogo.png';
  prodNombre: string;
  prodDocumento: string;
  menuCollapsed = true;

  ngOnInit() {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
    if (this.cobData.tipoUsuario !== 'productor') {
      this.router.navigate(['']);
    }
    // console.log(this.cobData.aRoute);
    this.prodNombre = this.cobData.usuarioNombre;
    this.prodDocumento = this.cobData.usuarioDocumento;
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  /* --------- Metodos Ruteables ------------------ */
  openAddForm(estadoVisita: number) {
    this.cobData.estadoInicial = estadoVisita;
    this.cobData.cameFromMain = true;
    this.goTo('agendaForm');
  }

  openAgenda(): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.goTo('agenda');
  }
  /* ---------------------------------------------- */

}
