import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainProductoresComponent } from './main-productores.component';

describe('MainProductoresComponent', () => {
  let component: MainProductoresComponent;
  let fixture: ComponentFixture<MainProductoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainProductoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainProductoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
