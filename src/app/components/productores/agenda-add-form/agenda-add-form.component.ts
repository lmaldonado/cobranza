import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, DoCheck, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { INgxMyDpOptions, IMyDateModel, IMyInputFieldChanged } from 'ngx-mydatepicker';
import { LoadingDarkOverlayComponent } from '../../../helper-directives/loading-dark-overlay/loading-dark-overlay.component';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-agenda-add-form',
  templateUrl: './agenda-add-form.component.html',
  styleUrls: ['./agenda-add-form.component.scss']
})
export class AgendaAddFormComponent implements OnInit, OnDestroy, DoCheck {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/agendaForm';
  homeComponentRoute = '/mainp';
  menuCollapsed = true;
  myDatePickerOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    dayLabels: { su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mie', th: 'Jue', fr: 'Vie', sa: 'Sab' },
    monthLabels: {
      1: 'Ene', 2: 'Feb', 3: 'Mar', 4: 'Abr', 5: 'May', 6: 'Jun',
      7: 'Jul', 8: 'Ago', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dic'
    },
    todayBtnTxt: 'Hoy'
  };

  nuevoRegistro: any;
  loading = false;
  //variable para pasar segun estado
  Dni:string;
  FechaN:any;
  Contrato: string;
  Importe: string;
  fechaV:string;

  // Param Properties
  paramNombre: string;
  paramTelefono: string;
  paramDireccion: string;
  paramFechaHora: string;
  paramObservaciones: string;
  paramFechaNacimiento: string;
  paramImporte: string;

  // Form Properties
  formNombre: string;
  formTelefono: string;
  formDireccion: string;
  formFecha: any;
  formTiempo = '';
  formHora = '0';
  formMinuto = '0';
  formObservaciones: string;
  formEstado: string;
  formDni: string;
  formNumeroContrato: string;
  formImporte: string;
  formFechaNacimiento: any;

  // fecha
  dia: string;
  mes: string;
  ano: string;

  // Geolocalización
  gpsTxt: any;
  gpsTxtLatitud: number;
  gpsTxtLongitud: number;
  gotCurrentLocation: boolean;

  ngOnInit() {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
    if (this.cobData.tipoUsuario !== 'productor') {
      this.router.navigate(['']);
    }

    console.log(this.cobData.aRoute);
    this.getCurrentLocation();
    this.formEstado = this.cobData.estadoInicial.toString();

  }

  ngDoCheck() {
    this.initiateMasks();
  }

  ngOnDestroy() {
    this.cobData.cameFromAgenda = undefined;
    this.cobData.estadoInicial = undefined;
    this.cobData.cameFromMain = undefined;
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  validateTextarea(field: string, e) {

    const REG_EX = /[^,;/\\\\*%&()'"]+/;
    this.formDireccion = this.formDireccion ? this.formDireccion : '';

    if (field === 'formDireccion' && !REG_EX.test(e.key)) {
      /*const el = document.getElementById('formDireccion');
      this.formDireccion = this.formDireccion.substring(0, this.formDireccion.length);*/
    }

  }

  initiateMasks() {
    let charLen: string;

    charLen = ''; for (let i = 0; i <= 25; i++) { charLen = charLen + 'S'; }
    // charLen = '0' + charLen;
    $('#formNombre').mask(charLen, {placeholder: 'Nombre...', 'translation': {S: {pattern: /[^,;/\\\\*%&()'"]+/}}});
    // $('#formNombre').mask('sssssssssssssssss 25  veces', {placeholder: 'Nombre...', 'translation': {S: {pattern: /[^,;/\\\\*%&()'"]+/}}});

    charLen = ''; for (let i = 0; i <= 15; i++) { charLen = charLen + 'S'; }
    $('#formDni').mask(charLen, {placeholder: 'DNI...', 'translation': {S: {pattern: /[^,;/\\\\*%&()'"]+/}}});

    charLen = ''; for (let i = 0; i <= 15; i++) { charLen = charLen + 'S'; }
    $('#formNumeroContrato').mask(charLen, {placeholder: 'Contrato...', 'translation': {S: {pattern: /[^,;/\\\\*%&()'"]+/}}});

    $('#formdp').mask('00-00-0000', {placeholder: '__-__-____'});
    $('#formFechaNacimiento').mask('00-00-0000', {placeholder: '__-__-____'});
    $('#formTelefono').mask('000-0000-0000000000', {placeholder: '000-0000-0000'});
    $('#formTiempo').mask('H0:M0', {placeholder: 'HH:MM', 'translation': {H: {pattern: /^[0-2]$/}, M: {pattern: /^[0-5]$/}}});

    const translationObject = {
      placeholder: '0000,00',
      'translation': {
        0: {pattern: /^[0-9]+$/},
        S: {pattern: /^[0-9,]+$/}
      }
    };
    $('#formImporte').mask('0SSSSSSSSSS0', translationObject);
  }

  getCurrentLocation() {
    this.cobData.getGpsTxt().subscribe((data) => {
      this.gpsTxt = data;
      const commaIndex = this.gpsTxt.indexOf(',');
      const lat = this.gpsTxt.slice(0, commaIndex);
      const lon = this.gpsTxt.slice(commaIndex + 1);
      this.gpsTxtLatitud = parseFloat(lat);
      this.gpsTxtLongitud = parseFloat(lon);
      this.gotCurrentLocation = true;
    });
  }

  onDateChanged(dateEvent: IMyDateModel): void {
    this.formFecha = dateEvent.formatted;
  }

  onDateChangedNacimiento(dateEvent: IMyDateModel): void {
    this.formFechaNacimiento = dateEvent.formatted;
  }

  validateForm() {

    const REG_EX = /[,;/\\\\*%&()'"]+/g;

    if (!this.formNombre) {
      ons.notification.alert('Debe Ingresar un nombre de cliente', {title: ''});
      return false;
    }

    if (!this.formTelefono) {
      this.formTelefono = null;
    }

    if (!this.formDireccion && this.formEstado !== '3') {
      ons.notification.alert('Debe Ingresar una dirección', {title: ''});
      return false;
    }

    if (!this.formFecha && (this.formEstado === '1' || this.formEstado === '4')) {
      ons.notification.alert('Debe Ingresar una fecha de visita valida', {title: ''});
      return false;
    }

    /*if ((this.formHora === '0' || this.formMinuto === '0') && (this.formEstado === '1' || this.formEstado === '4')) {
      ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
      return false;
    }*/

    if (this.formTiempo.length !== 5 && (this.formEstado === '1' || this.formEstado === '4')) {
      ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
      this.formTiempo = '';
      return false;
    } else if (this.formTiempo.length === 5 && (this.formEstado === '1' || this.formEstado === '4')) {
      const h = parseFloat(this.formTiempo.substr(0, 2));
      const m = parseFloat(this.formTiempo.substr(3, 4));
      if (h > 23 || m > 59) {
        ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
        this.formTiempo = '';
        return false;
      } else {
        this.formHora = this.formTiempo.substr(0, 2);
        this.formMinuto = this.formTiempo.substr(3, 4);
      }
    }

    if (!this.formObservaciones) {
      this.formObservaciones = null;
    } else {
      this.paramObservaciones = this.formObservaciones.replace(REG_EX, ' ');
    }

    if (!this.formDni && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar el DNI del cliente.', {title: ''});
      return false;
    }

    if (!this.formNumeroContrato && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar el número de contrato.', {title: ''});
      return false;
    }

    if (!this.formImporte && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar importe del contrato.', {title: ''});
      return false;
    } else if (this.formImporte && this.formEstado === '2') {
      let countSeparators = 0;
      for (let i = 0; i < this.formImporte.length; i++) {
        if (this.formImporte.charAt(i) === ',') { countSeparators++; }
      }
      if (countSeparators > 1) {
        ons.notification.alert('El importe debe ser de la forma 0000,00', {title: ''});
        return false;
      }
      if (this.formImporte.charAt(this.formImporte.length - 1) === ',') {
        ons.notification.alert('El importe debe ser de la forma 0000,00', {title: ''});
        return false;
      }
      this.paramImporte = this.formImporte.replace(',', '.');
    }

    if (!this.formFechaNacimiento && this.formEstado === '2') {
      ons.notification.alert('Debe ingresar una fecha de nacimiento valida', {title: ''});
      return false;
    }

    this.paramNombre = this.formNombre;
    this.paramTelefono = this.formTelefono;
    this.paramDireccion = this.formDireccion.replace(REG_EX, ' ');

    if (this.formFecha) {
      this.dia = this.formFecha.date.day >= 10 ? `${this.formFecha.date.day}` : `0${this.formFecha.date.day}`;
      this.mes = this.formFecha.date.month >= 10 ? `${this.formFecha.date.month}` : `0${this.formFecha.date.month}`;
      this.ano = this.formFecha.date.year.toString();
      this.paramFechaHora = `${this.ano}-${this.mes}-${this.dia} ${this.formHora}:${this.formMinuto}:00`;
    }

    if (this.formFechaNacimiento) {
      this.dia = this.formFechaNacimiento.date.day >= 10 ? `${this.formFechaNacimiento.date.day}` : `0${this.formFechaNacimiento.date.day}`;
      this.mes = this.formFechaNacimiento.date.month >= 10 ? `${this.formFechaNacimiento.date.month}` : `0${this.formFechaNacimiento.date.month}`;
      this.ano = this.formFechaNacimiento.date.year.toString();
      this.paramFechaNacimiento = `${this.ano}-${this.mes}-${this.dia}`;
    }

    return true;
  }

  /* --------- Metodos Ruteables ------------------ */
  agendarCliente() {
    this.loading = true;

    if (this.validateForm()) {
      this.Dni=null; 
      this.FechaN=null;
      this.Contrato=null;
      this.Importe=null;
      
      this.fechaV = this.paramFechaHora;
      if (this.formEstado === '2') {
         this.Dni=this.formDni;
         this.FechaN=this.paramFechaNacimiento;
         this.Contrato=this.formNumeroContrato;
         this.Importe= this.paramImporte;
         this.fechaV= null; 
        }
      const paramsArray = [{
        idUsuario: this.cobData.loggedUserId.toString(),
        idEstadoVisita: this.formEstado.toString(),
        nombre: this.paramNombre,
        telefono: this.paramTelefono,
        direccion: this.paramDireccion,
        fechahora: this.fechaV,
        observaciones: this.paramObservaciones,
        latitud: this.gpsTxtLatitud.toString(),
        longitud: this.gpsTxtLongitud.toString(),        
        dni: this.Dni,
        contrato: this.Contrato,
        importe: this.Importe,
        fechaNacimiento: this.FechaN,
        idCobranza : null,
        idPoliza : null 
      }];
       console.log(paramsArray);

      this.cobData.postClienteAgenda(paramsArray)
        .subscribe((data) => {
          this.nuevoRegistro = data[0];
          this.loading = false;
          ons.notification.alert(`Cliente ${this.nuevoRegistro.NombreCliente} Agregado a la agenda!`, {title: ''});
          this.goBack();
        });
    } else {
      this.loading = false;
    }

  }
  /* ---------------------------------------------- */

}
