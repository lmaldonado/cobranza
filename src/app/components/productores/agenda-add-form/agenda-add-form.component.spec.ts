import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaAddFormComponent } from './agenda-add-form.component';

describe('AgendaAddFormComponent', () => {
  let component: AgendaAddFormComponent;
  let fixture: ComponentFixture<AgendaAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaAddFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
