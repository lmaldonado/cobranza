import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-agenda-list',
  templateUrl: './agenda-list.component.html',
  styleUrls: ['./agenda-list.component.scss']
})
export class AgendaListComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/agenda';
  homeComponentRoute = '/mainp';
  clientes: any;
  menuCollapsed = true;
  noData: boolean;
  loading = false;
  statusClass = '';

  // Geolocalización
  gpsTxt: any;
  gpsTxtLatitud: number;
  gpsTxtLongitud: number;
  gotCurrentLocation: boolean;

  ngOnInit() {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
    if (this.cobData.tipoUsuario !== 'productor') {
      this.router.navigate(['']);
    }

    // console.log(this.cobData.aRoute);
    this.noData = false;
    try {
      this.cobData.getClientesAgenda().subscribe((data) => {
        const betaClientes = data;
        // this.clientes = data;
        if (data.noData) {
          this.noData = true;
        } else {
          this.getCurrentLocation().then((resolvedData) => {
            // get distances
            let distance: number;
            betaClientes.forEach((e) => {
              distance = this.calculateUserDistanceWith(parseFloat(e.LatitudCliente), parseFloat(e.LongitudCliente));
              e.distance = distance;
            });
            this.clientes = this.sortByKey(betaClientes, 'distance');
            // console.log(this.clientes);
          });
        }

      });
    } catch (error) {
      console.log(error);
    }

  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  cerrarSesion(): void {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  sortGps(): void {
    this.loading = true;
    this.getCurrentLocation().then((resolvedData) => {
      // get distances
      let distance: number;
      this.clientes.forEach((e) => {
        distance = this.calculateUserDistanceWith(parseFloat(e.LatitudCliente), parseFloat(e.LongitudCliente));
        e.distance = distance;
      });
      this.clientes = this.sortByKey(this.clientes, 'distance');
      this.loading = false;
      // console.log(this.clientes);
    });
  }

  getCurrentLocation(): Promise<any> {
    const q = new Promise((resolve, reject) => {
      this.cobData.getGpsTxt().subscribe((data) => {
        this.gpsTxt = data;
        const commaIndex = this.gpsTxt.indexOf(',');
        const lat = this.gpsTxt.slice(0, commaIndex);
        const lon = this.gpsTxt.slice(commaIndex + 1);
        this.gpsTxtLatitud = parseFloat(lat);
        this.gpsTxtLongitud = parseFloat(lon);
        resolve(this.gotCurrentLocation = true);
      });
    });
    return q;
  }

  sortByKey(array, key): any[] {
    return array.sort((a, b) => {
        const x = a[key]; const y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

  calculateUserDistanceWith(lat: number, lon: number): number {
    const dx = this.gpsTxtLatitud - lat;
    const dy = this.gpsTxtLongitud - lon;
    return parseFloat((Math.sqrt((Math.pow(dx, 2)) + (Math.pow(dy, 2))) * 100000).toFixed(0));
  }

  /* --------- Metodos Ruteables ------------------ */
  seeMap(index: number): void {
    const gpsObject = {
      lat: parseFloat(this.clientes[index].LatitudCliente),
      lon: parseFloat(this.clientes[index].LongitudCliente),
      diasDiferencia: parseInt(this.clientes[index].DiferenciaFecha, 10)
    };
    this.cobData.gpsAgendados = gpsObject;
    this.cobData.flagClienteMap = true;
    this.goTo('agenda-map');
  }

  openAddForm(estadoVisita: number): void {
    this.cobData.estadoInicial = estadoVisita;
    this.cobData.cameFromAgenda = true;
    this.goTo('agendaForm');
  }

  selectCliente(idRegistro: number): void {
    // this.cobData.clienteSeleccionado = this.clientes[index];
    this.cobData.IdRegistroAgenda = idRegistro;
    this.goTo('agendaDetalle');
  }

  verUbicacionAgendados(): void {
    let gpsArray = [];
    this.clientes.forEach( (e) => {
      gpsArray.push({
        lat: parseFloat(e.LatitudCliente),
        lon: parseFloat(e.LongitudCliente),
        diasDiferencia: parseInt(e.DiferenciaFecha, 10),
        cliente: e.NombreCliente,
        telefono: e.Telefono,
        domicilio: e.Direccion,
        idRegistro: e.IdRegistro
      });
    });
    this.cobData.gpsAgendados = gpsArray;
    this.cobData.flagPendientesMap = true;
    this.goTo('agenda-map');
  }
  /* ------------------------------------------------*/

}
