import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaMapComponent } from './agenda-map.component';

describe('AgendaMapComponent', () => {
  let component: AgendaMapComponent;
  let fixture: ComponentFixture<AgendaMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
