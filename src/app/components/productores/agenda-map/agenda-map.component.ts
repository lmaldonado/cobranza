import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, AfterViewInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-agenda-map',
  templateUrl: './agenda-map.component.html',
  styleUrls: ['./agenda-map.component.scss']
})
export class AgendaMapComponent implements OnInit, OnDestroy {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/agenda-map';
  homeComponentRoute = '/mainp';
  menuCollapsed = true;
  pageTitle: string;
  gpsTxt: any;
  gpsAgendados: any;
  currentLatitude: number;
  currentLongitude: number;
  flagPendientesMap: boolean;
  flagClienteMap: boolean;
  currentCliente: string;
  currentDomicilio: string;
  currentTelefono: string;
  lastId = -1; // handler para el doble click ver detalle en verDatos()
  markers = {
    red: '../../../../assets/maps-markers/marker-red.png',
    redDot: '../../../../assets/maps-markers/marker-red-dot.png',
    redSm: '../../../../assets/maps-markers/marker-red-sm.png',
    redDotSm: '../../../../assets/maps-markers/marker-red-dot-sm.png',
    yellow: '../../../../assets/maps-markers/marker-yellow.png',
    yellowDot: '../../../../assets/maps-markers/marker-yellow-dot.png',
    yellowSm: '../../../../assets/maps-markers/marker-yellow-sm.png',
    yellowDotSm: '../../../../assets/maps-markers/marker-yellow-dot-sm.png',
    green: '../../../../assets/maps-markers/marker-green.png',
    greenDot: '../../../../assets/maps-markers/marker-green-dot.png',
    greenSm: '../../../../assets/maps-markers/marker-green-sm.png',
    greenDotSm: '../../../../assets/maps-markers/marker-green-dot-sm.png',
    blue: '../../../../assets/maps-markers/marker-blue.png',
    blueDot: '../../../../assets/maps-markers/marker-blue-dot.png',
    blueSm: '../../../../assets/maps-markers/marker-blue-sm.png',
    blueDotSm: '../../../../assets/maps-markers/marker-blue-dot-sm.png',
    gray: '../../../../assets/maps-markers/marker-gray.png',
    grayDot: '../../../../assets/maps-markers/marker-gray-dot.png',
    graySm: '../../../../assets/maps-markers/marker-gray-sm.png',
    grayDotSm: '../../../../assets/maps-markers/marker-gray-dot-sm.png',
    darkgray: '../../../../assets/maps-markers/marker-darkgray.png',
    darkgrayDot: '../../../../assets/maps-markers/marker-darkgray-dot.png',
    darkgraySm: '../../../../assets/maps-markers/marker-darkgray-sm.png',
    darkgrayDotSm: '../../../../assets/maps-markers/marker-darkgray-dot-sm.png',
    orange: '../../../../assets/maps-markers/marker-orange.png',
    orangeDot: '../../../../assets/maps-markers/marker-orange-dot.png',
    orangeSm: '../../../../assets/maps-markers/marker-orange-sm.png',
    orangeDotSm: '../../../../assets/maps-markers/marker-orange-dot-sm.png'
  };

  ngOnInit() {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }

    // console.log(this.cobData.aRoute);
    this.getCurrentLocation();
    this.flagPendientesMap = this.cobData.flagPendientesMap;
    this.flagClienteMap = this.cobData.flagClienteMap;
    this.gpsAgendados = this.cobData.gpsAgendados;
    console.log(this.gpsAgendados);
    // this.pageTitle = ((this.flagClienteMap) ? 'Ubicación Cliente' : 'Visitas Pendientes');

    if (this.flagPendientesMap) {
      this.gpsAgendados.forEach((e) => {
        if (e.diasDiferencia === 0) { e.marker = this.markers.greenSm; } else
          if (e.diasDiferencia === 1) { e.marker = this.markers.yellowSm; } else
            if (e.diasDiferencia >= 2) { e.marker = this.markers.redSm; } else { e.marker = this.markers.darkgraySm; }
      });
    }

    this.currentCliente = ((this.flagClienteMap) ? 'Ubicación Cliente' : 'Visitas Pendientes');
    this.currentDomicilio = '';
    this.currentTelefono = '';
    console.log(this.flagClienteMap);
    console.log('linea 95');
    if (this.flagClienteMap) {    
      this.currentCliente = this.gpsAgendados.cliente;
      this.currentDomicilio = this.gpsAgendados.direccion;
      this.currentTelefono = this.gpsAgendados.telefono;
    }
    // console.log(this.gpsAgendados);
    // console.log(`Pendientes: ${this.flagPendientesMap} Cliente: ${this.flagClienteMap}`);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  ngOnDestroy() {
    // this.cobData.flagPendientesMap = undefined;
    // this.cobData.flagClienteMap = undefined;
    // this.gpsAgendados = undefined;
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  getCurrentLocationOLD() {

    this.cobData.getGpsTxt().subscribe((data) => {
      this.gpsTxt = data;
      const commaIndex = this.gpsTxt.indexOf(',');
      const lat = this.gpsTxt.slice(0, commaIndex);
      const lon = this.gpsTxt.slice(commaIndex + 1);
      this.currentLatitude = parseFloat(lat);
      this.currentLongitude = parseFloat(lon);
      // console.log(`lat: ${lat} / lon: ${lon}`);
    });
  }

  getCurrentLocation(): Promise<any> {
    const q = new Promise((resolve, reject) => {
      this.cobData.getGpsTxt().subscribe((data) => {
        const gpsTxt = data;
        const commaIndex = gpsTxt.indexOf(',');
        const lat = gpsTxt.slice(0, commaIndex);
        const lon = gpsTxt.slice(commaIndex + 1);
        this.currentLatitude = parseFloat(lat);
        this.currentLongitude = parseFloat(lon);
        resolve();
      });
    });
    return q;
  }

  actualizarGPS() {
    this.getCurrentLocation().then(() => {
      ons.notification.toast('Ubicación actualizada', {timeout: 2000});
    });
  }

  /* --------- Metodos Ruteables ------------------ */
  verInfo(domicilio: any) {
    console.log(domicilio);

    if (domicilio.idRegistro === this.lastId) {
      this.cobData.IdRegistroAgenda = domicilio.idRegistro;
      this.goTo('agendaDetalle');
    } else {
      this.lastId = domicilio.idRegistro;
      this.currentCliente = domicilio.cliente;
      this.currentTelefono = `Tlf. ${domicilio.telefono}`;
      this.currentDomicilio = domicilio.domicilio;
    }
  }
  /* ---------------------------------------------- */

}
