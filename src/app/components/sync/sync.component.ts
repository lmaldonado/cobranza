import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  OnInit
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { Router } from '@angular/router';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../services/cobdata.service';
import * as ons from 'onsenui';

@Component({
  selector: 'app-sync',
  templateUrl: './sync.component.html',
  styleUrls: ['./sync.component.scss']
})
export class SyncComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }
  appLogo = '/../../../assets/appLogo.png';
  
  icono : string;
      icono2 :string;
      icono3: string;
      icono4 : string;
      icono41: string;
      icono42: string;
      icono43:string;
      icono44:string;
      icono45:string;
      icono46:string;
      icono47:string;
      icono48:string;
      icono49:string;
      icono410:string;
      siguiente=10;
      completo=0;
  inicio = 'Sincronización..';
 
  id:number;
  componentRoute = '/Sync';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  finalizar = true;
  cobrador = false;
  ngOnInit() {
     this.validateSession();
  }
  Sync(){
      this.cobrador=true;
      this.inicio = 'Sincronización en curso';
       this.icono = 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
      this.icono2 = 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono3= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono4= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono41= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono42= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono43= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono44= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
      this.icono45= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono46= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono47= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono48= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono49= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.icono410= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
       this.id=this.cobData.cobradorId;
       this.cobData.SyncCobradorUsu(this.id).subscribe((data)=>{
            this.icono41 = 'fa fa-check-square';
            this.siguiente_bolck(1);


          });
          this.cobData.SyncCobrador(this.id).subscribe((data)=>{
            this.icono42 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.cobData.SyncCobradorOrd(this.id).subscribe((data)=>{
            this.icono43 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.cobData.SyncCobradorPol(this.id).subscribe((data)=>{
            this.icono44 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.cobData.SyncCobradorEst(this.id).subscribe((data)=>{
            this.icono45 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.cobData.SyncCobradorTit(this.id).subscribe((data)=>{
            this.icono46 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.cobData.SyncCobradorCobCob(this.id).subscribe((data)=>{
            this.icono47 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.cobData.SyncCobradorCobCobOrd(this.id).subscribe((data)=>{
            this.icono48 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.cobData.SyncCobradorPgo(this.id).subscribe((data)=>{
            this.icono49 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.cobData.SyncCobradorRen(this.id).subscribe((data)=>{
            this.icono410 = 'fa fa-check-square';
            this.siguiente_bolck(1);

          });
          this.icono4 = 'fa fa-check-square';
  }
   siguiente_bolck(control:number) {
     
      this.completo = this.completo + control;
      console.log(this.completo,control);

     if (this.completo == this.siguiente){
         this.finalizar = false;
        this.cobrador = true;
     }

   }
   validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }
   goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }
  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

}
