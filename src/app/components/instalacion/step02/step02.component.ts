import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  OnInit
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { Router } from '@angular/router';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import * as ons from 'onsenui';

@Component({
  selector: 'app-step02',
  templateUrl: './step02.component.html',
  styleUrls: ['./step02.component.scss']
})
export class Step02Component implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  appLogo = '../../../../assets/appLogo.png';
  cobrador: any;
  icono = 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono2 = 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono3= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono4= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono41= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono42= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono43= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono44= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono45= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono46= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono47= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono48= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono49= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  icono410= 'fa fa-spinner fa-pulse fa-fw app-icon-loading m-1';
  dni: string;
  id:number;
  id2:number;
  nombre:string;
  pass:string;
  alias:string;
  tipo:string;
  ambos=false;


  ngOnInit() {
    if (!this.cobData.cobradorToSync) {
      this.router.navigate(['step-01']);
    }

    this.cobrador = this.cobData.cobradorToSync;
    this.cobData.wizard01().subscribe((data) => {
      this.icono = 'fa fa-check-square';
      this.cobData.wizard02().subscribe((data) => {
        this.icono2 = 'fa fa-check-square';
        console.log(this.cobrador);
        if (this.cobrador.NombreProductor==undefined){
          this.cobData.productoresRemoto(this.cobrador.Documento).subscribe((data)=>{  
          if (data.error){  
          console.log('linea-65');             
          }else{
            console.log('linea-67');             
            this.id2=data[0].IdProductor
            this.f_productor('productor',this.id2,'p')
          }
          });
          console.log('linea-72');             
          this.id=this.cobrador.IdCobrador;
          this.nombre=this.cobrador.NombreCobrador;
          this.alias=this.cobrador.alias;
          this.dni=this.cobrador.Documento;        
          this.pass=this.cobrador.Documento; 
          this.f_cobrador('cobrador',this.id)
        }else{
          this.cobData.cobradoresRemoto(this.cobrador.Documento).subscribe((data)=>{
          if (data.error){      
          console.log('linea-82');                      
          }else{
            this.id2=data[0].IdProductor
            this.f_cobrador('cobrador',this.id2)
            console.log('linea-86');             
          }
          });
          console.log('linea-89');             
          this.id=this.cobrador.IdProductor;
          this.nombre=this.cobrador.NombreProductor;
          this.alias=this.cobrador.alias;
          this.tipo='productor';
          this.dni=this.cobrador.Documento;        
          this.pass=this.cobrador.Documento; 
          this.f_productor('productor',this.id,'p')
        }            
      
      });
        
      });
   }
  f_cobrador(tipo:string,id:number){
    this.cobData.ingresaLogin(this.dni, id,this.nombre,this.pass,this.alias,tipo).subscribe((data)=>{
          this.icono3 = 'fa fa-check-square'; 
          console.log('linea-106',this.tipo);
          if (tipo=='cobrador'){
            this.cobData.SyncCobradorUsu(id).subscribe((data)=>{
              this.icono41 = 'fa fa-check-square';

            });
            this.cobData.SyncCobrador(id).subscribe((data)=>{
              this.icono42 = 'fa fa-check-square';

            });
            this.cobData.SyncCobradorOrd(id).subscribe((data)=>{
              this.icono43 = 'fa fa-check-square';

            });
            this.cobData.SyncCobradorPol(id).subscribe((data)=>{
              this.icono44 = 'fa fa-check-square';

            });
            this.cobData.SyncCobradorEst(id).subscribe((data)=>{
              this.icono45 = 'fa fa-check-square';

            });
            this.cobData.SyncCobradorTit(id).subscribe((data)=>{
              this.icono46 = 'fa fa-check-square';

            });
            this.cobData.SyncCobradorCobCob(id).subscribe((data)=>{
              this.icono47 = 'fa fa-check-square';

            });
            this.cobData.SyncCobradorCobCobOrd(id).subscribe((data)=>{
              this.icono48 = 'fa fa-check-square';

            });
            this.cobData.SyncCobradorPgo(id).subscribe((data)=>{
              this.icono49 = 'fa fa-check-square';

            });
            this.cobData.SyncCobradorRen(id).subscribe((data)=>{
              this.icono410 = 'fa fa-check-square';

            });

          }
          this.icono4 = 'fa fa-check-square';

        });
  }
  f_productor(tipo:string,id:number,aliasp:string){
    aliasp=aliasp + this.alias;
    this.cobData.ingresaLogin(this.dni, id,this.nombre,this.pass,aliasp,tipo).subscribe((data)=>{
          this.icono3 = 'fa fa-check-square'; 

          if (tipo=='productor'){
            this.cobData.SyncProductorUsu(id).subscribe((data)=>{
              this.icono41 = 'fa fa-check-square';

            });
            this.cobData.SyncProductor(id).subscribe((data)=>{
              this.icono42 = 'fa fa-check-square';

            });
            this.cobData.SyncAppEstadoV(id).subscribe((data)=>{
              this.icono42 = 'fa fa-check-square';

            });          
          }
          this.icono4 = 'fa fa-check-square';

        });
  }

}
