import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  OnInit
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { Router } from '@angular/router';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import * as ons from 'onsenui';

@Component({
  selector: 'app-step01',
  templateUrl: './step01.component.html',
  styleUrls: ['./step01.component.scss']
})
export class Step01Component implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  formDni: string;
  formDb: string;
  formUs: string;
  formli:string;
  formTipoUsuario = 'cobrador';
  appLogo = '../../../../assets/appLogo.png';
  cobrador: any;
  showLoading = false;
  bd = false;
  licencia=false;
  cob_prod:string;

  ngOnInit() {
  }
  Setea(set:string):void{
    this.cobrador = undefined;
    console.log();

  }
  verificar(): void {

    if (this.formDni ) {
      if(this.formTipoUsuario === 'cobrador'){
        this.cobrador = undefined;
        this.showLoading = true;
        this.cobData.validarCobrador(this.formDni).subscribe((data) => {
          if (data.noData) {
            ons.notification.alert(`DNI: ${this.formDni} no existe en la base de datos.`, {title: ''});
            this.showLoading = false;
          } else {
            this.cobrador = data[0];
            this.cob_prod='Cobrador';
            // console.log(this.cobrador);
            this.showLoading = false;
          }
        });
      }else{
        this.cobrador = undefined;
        this.showLoading = true;
        this.cobData.validarProductor(this.formDni).subscribe((data) => {
          if (data.noData) {
            ons.notification.alert(`DNI: ${this.formDni} no existe en la base de datos.`, {title: ''});
            this.showLoading = false;
          } else {
            this.cobrador = data[0];
            this.cob_prod='Productor';
             console.log(this.cobrador);
            this.showLoading = false;
          }
        });

      }
    } else {
      ons.notification.toast('Ingrese todos los datos requeridos...', {timeout: 2000});
      this.showLoading = false;
    }
  }

  siguiente(): void {
    this.cobData.cobradorToSync = this.cobrador;
    
    this.router.navigate(['step-02']);
  }
  writeINI(): void {
    this.cobData.writeINI( this.formDb,this.formUs).subscribe((data) => {
      this.bd=true;
      this.licencia=true;
    });
  }
  licenciamiento(): void {
    this.showLoading = true;
    if (!this.formli){
      console.log('data');
      this.cobData.licencia( 'vacio').subscribe((data) => {      
        console.log(data);
        if (data.status=='Active'){
        this.showLoading = false;
        this.licencia = false;
        }

      });

    }else{
      this.cobData.licencia( this.formli).subscribe((data) => {
      console.log(data);   


        if (data.status=='Active'){
        this.showLoading = false;
        this.licencia = false;
        }
      });
    }
  }

}
