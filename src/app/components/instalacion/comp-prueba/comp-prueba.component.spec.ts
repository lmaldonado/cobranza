import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompPruebaComponent } from './comp-prueba.component';

describe('CompPruebaComponent', () => {
  let component: CompPruebaComponent;
  let fixture: ComponentFixture<CompPruebaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompPruebaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompPruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
