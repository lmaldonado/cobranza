import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, DoCheck, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { LoadingDarkOverlayComponent } from '../../../helper-directives/loading-dark-overlay/loading-dark-overlay.component';

@Component({
  selector: 'app-comp-prueba',
  templateUrl: './comp-prueba.component.html',
  styleUrls: ['./comp-prueba.component.scss']
})
export class CompPruebaComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/ruta-prueba';
  homeComponentRoute = '/main';  // ruta del componente principal  (cobradores o productores)
  menuCollapsed = true;
  nombre =  'Jose';
  edad = 20;

  ngOnInit() {

  }

  saludar(nombre){
  	ons.notification.alert(`hola ${nombre}`);
  }


}
