import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-ruta',
  templateUrl: './ruta.component.html',
  styleUrls: ['./ruta.component.scss']
})
export class RutaComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }
   componentRoute = '/clienteMap';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  gps: any;
  longitud:any;
  latitud:any;
  rutas:any;
  currentFecha:any;
  

  displayMap: boolean;
  initialZoom = 15;
  fromAgenda: boolean;
  markers = {
    red: '../../../assets/maps-markers/marker-red.png',
    redDot: '../../../assets/maps-markers/marker-red-dot.png',
    yellow: '../../../assets/maps-markers/marker-yellow.png',
    yellowDot: '../../../assets/maps-markers/marker-yellow-dot.png',
    green: '../../../assets/maps-markers/marker-green.png',
    greenDot: '../../../assets/maps-markers/marker-green-dot.png',
    blue: '../../../assets/maps-markers/marker-blue.png',
    blueDot: '../../../assets/maps-markers/marker-blue-dot.png',
    gray: '../../../assets/maps-markers/marker-gray.png',
    grayDot: '../../../assets/maps-markers/marker-gray-dot.png',
    orange: '../../../assets/maps-markers/marker-orange.png',
    orangeDot: '../../../assets/maps-markers/marker-orange-dot.png'
  };


  ngOnInit() { 
  		this.validateSession();
  		

	    console.log(this.cobData.aRoute);
	    this.fromAgenda = this.cobData.clienteFromAgenda;
	    
	    this.longitud=parseFloat(this.cobData.longitud);
	    this.latitud=parseFloat(this.cobData.latitud);
	    console.log(this.cobData.longitud);
	    console.log(this.longitud);

	    this.rutas = this.cobData.rutas;
	    console.log(this.rutas);


	    this.displayMap = true;
      if (navigator.geolocation){
          
        navigator.geolocation.getCurrentPosition((position) => {
         
          
        })

      }else{
        ons.notification.toast('no soporta ubicacion', {timeout: 2000});
      }

	  }

	  validateSession(): void {
	    if (!this.cobData.loggedUserId) {
	      this.router.navigate(['']);
	    }
	  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  getCurrentLocation(): Promise<any> {
    const q = new Promise((resolve, reject) => {
      this.cobData.getGpsTxt().subscribe((data) => {
        const gpsTxt = data;
        const commaIndex = gpsTxt.indexOf(',');
        const lat = gpsTxt.slice(0, commaIndex);
        const lon = gpsTxt.slice(commaIndex + 1);
        this.gps.cLat = parseFloat(lat);
        this.gps.cLon = parseFloat(lon);
        this.displayMap = true;
        resolve();
      });
    });
    return q;
  }

  actualizarGPS(): void {
    this.getCurrentLocation().then(() => {
      ons.notification.toast('Ubicación actualizada', {timeout: 2000});
    });
  }
  


  }


