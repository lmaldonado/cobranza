import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RendicionesListComponent } from './rendiciones-list.component';

describe('RendicionesListComponent', () => {
  let component: RendicionesListComponent;
  let fixture: ComponentFixture<RendicionesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RendicionesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RendicionesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
