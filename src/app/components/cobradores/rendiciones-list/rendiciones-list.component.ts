import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-rendiciones-list',
  templateUrl: './rendiciones-list.component.html',
  styleUrls: ['./rendiciones-list.component.scss']
})
export class RendicionesListComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/listado-rendiciones';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  idCobranza: number;
  licenciaCliente: string;
  rendiciones: any;
  noData = false;

  ngOnInit() {

    this.validateSession();

    console.log(this.cobData.aRoute);
    this.idCobranza = this.cobData.cobranzaId;
    this.licenciaCliente = this.cobData.licenciaCliente;
    this.cobData.getRendicionListado(this.idCobranza, this.licenciaCliente)
    .subscribe((data) => {
      if (data.error) {
        this.noData = true;
      } else {
        this.rendiciones = data;
      }
    });
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  /* --------- Metodos Ruteables ------------------ */
  verDetalleRendicion(idRendicion: any) {
    this.cobData.flagRendicion = true;
    this.cobData.idRendicion = parseInt(idRendicion, 10);
    this.goTo('detalle-rendicion');
  }

  verPendientesNoAcreditadas() {
    this.cobData.flagNoAcreditadas = true;
    this.goTo('detalle-rendicion');
  }

  verPendientesAcreditadas() {
    this.cobData.flagAcreditadas = true;
    this.goTo('detalle-rendicion');
  }
  /* ------------------------------------------------*/

}
