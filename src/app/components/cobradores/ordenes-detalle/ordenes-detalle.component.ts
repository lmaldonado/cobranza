import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, AfterViewInit, OnChanges, DoCheck, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-ordenes-detalle',
  templateUrl: './ordenes-detalle.component.html',
  styleUrls: ['./ordenes-detalle.component.scss']
})
export class OrdenesDetalleComponent implements OnInit, OnDestroy {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/ordenesDetalle';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  ordenes: any;
  selectedOrdenes = [];
  totalImporte = 0;
  idPoliza: number;
  idCobranza: number;
  cameFromAgenda: boolean;
  cameFromPendientesMap: boolean;
  cameFromOrdenesList: boolean;
  loading = false;
  notAllCredited: boolean;
  IdRegistroAgenda: number;

  flagAcreditar = false;
  flagDesacreditar = false;
  acreditar = 0;
  distancia: string;

  gpsTxt: any;
  gps: any;

  currentLatitude: number;
  currentLongitude: number;
  gotCurrentLocation = false;
  domicilioLatitude: number;
  domicilioLongitude: number;

  ngOnInit() {

    this.validateSession();

    // console.log(this.cobData.aRoute);
    this.idPoliza = this.cobData.polizaId;
    this.idCobranza = this.cobData.cobranzaId;

    this.reloadOrdenes();

    this.cameFromAgenda = this.cobData.cameFromAgenda;
    this.cameFromPendientesMap = this.cobData.cameFromPendientesMap;
    this.cameFromOrdenesList = this.cobData.cameFromOrdenesList;

  }

  ngOnDestroy() {
    // this.cobData.cameFromAgenda = false;
    // this.cobData.cameFromOrdenesList = false;
    // this.cobData.cameFromPendientesMap = false;
  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  calculateUserDistanceWith(lat: number, lon: number): number {
    const dx = this.currentLatitude - lat;
    const dy = this.currentLongitude - lon;
    const res = parseFloat((Math.sqrt((Math.pow(dx, 2)) + (Math.pow(dy, 2))) * 100000).toFixed(0));
    if (isNaN(res)) {
      return -1;
    } else {
      return res;
    }
  }

  validarEliminarAgenda() {
    const cantidadOrdenes = this.ordenes.length;
    const tieneAgendado = this.ordenes[0].IdRegistroAgenda ? true : false;
    let cantidadAcreditadas = 0;

    this.ordenes.forEach(element => {

      if (element.IdEstado === '5') {
        cantidadAcreditadas++;
      }
    });
    // console.log(`a:${cantidadAcreditadas} o:${cantidadOrdenes} agenda?: ${tieneAgendado}`);
    if ((cantidadAcreditadas === cantidadOrdenes) && tieneAgendado) {
      this.cobData.deleteClienteAgenda(this.ordenes[0].IdRegistroAgenda).subscribe((data) => {
        ons.notification.alert('El registro en la agenda del cliente ha sido eliminado', {title: ''});
      });
    }
    if (cantidadAcreditadas !== cantidadOrdenes) {
      
      if (cantidadOrdenes - cantidadAcreditadas > 1) {
        ons.notification.alert(`Quedan ${cantidadAcreditadas} ordenes pendientes en la cuenta.`, {title: ''});
      } else {
        ons.notification.alert(`Queda ${cantidadAcreditadas} orden pendiente en la cuenta.`, { title: '' });
      }
    }

  }

  reloadOrdenes() {
    // console.log(`${this.idCobranza} - ${this.idPoliza}`);
    this.cobData.getOrdenesPolizaDetalle(this.idCobranza, this.idPoliza)
    .subscribe((data) => {
      this.ordenes = data;
      if (this.ordenes[0].IdRegistroAgenda) {
        this.IdRegistroAgenda = parseInt(this.ordenes[0].IdRegistroAgenda, 10);
      }
      this.domicilioLatitude = parseFloat(this.ordenes[0].LatitudDomicilio);
      this.domicilioLongitude = parseFloat(this.ordenes[0].LongitudDomicilio);
      
      this.getCurrentLocation().then(() => {
        const d = this.calculateUserDistanceWith(this.domicilioLatitude, this.domicilioLongitude);        
        if (d >= 0) {          
          this.distancia = d.toString() + 'm';     
          
        } else {
          this.distancia = 'N/D';
        }
      });
      this.setNotAllCreditedStatus();
    });
  }

  deleteClienteAgenda() {
    const msg = `Esta seguro que desea eliminar de la agenda al cliente de la cuenta actual?`;
    ons.notification.confirm(msg, { title: '', buttonLabels: ['Cancelar', 'Eliminar'] })
      .then((res) => {
        if (res) {
          this.loading = true;
          this.cobData.deleteClienteAgenda(this.IdRegistroAgenda).toPromise()
            .then((data) => {
              ons.notification.alert(`El registro de la agenda ha sido eliminado.`, { title: '' });
              this.loading = false;
              this.IdRegistroAgenda = undefined;
              this.reloadOrdenes();
            });
        } else {
          this.loading = false;
        }
      });
  }

  getCurrentLocationOLD() {

    this.cobData.getGpsTxt().subscribe( (data) => {
      this.gpsTxt = data;
      const commaIndex = this.gpsTxt.indexOf(',');
      const lat = this.gpsTxt.slice(0, commaIndex);
      const lon = this.gpsTxt.slice(commaIndex + 1);
      this.currentLatitude = parseFloat(lat);
      this.currentLongitude = parseFloat(lon);
      this.gotCurrentLocation = true;
    });

  }

  getCurrentLocation(): Promise<any> {
    const q = new Promise((resolve, reject) => {
      this.cobData.getGpsTxt().subscribe((data) => {
        this.gpsTxt = data;
        const commaIndex = this.gpsTxt.indexOf(',');
        const lat = this.gpsTxt.slice(0, commaIndex);
        const lon = this.gpsTxt.slice(commaIndex + 1);
        this.currentLatitude = parseFloat(lat);
        this.currentLongitude = parseFloat(lon);
        resolve(this.gotCurrentLocation = true);
      });
    });
    return q;
  }

  actualizarGPS() {
    const msg = `Esta seguro que desea actualizar la ubicación del cliente ${this.ordenes[0].Titular} con su ubicación actual?`;
    const confirmMsg = `La ubicación del cliente ${this.ordenes[0].Titular} se ha actualizado con exito!`;
    ons.notification.confirm(msg, {title: '', buttonLabels: ['No', 'Si']})
    .then((res) => {
      this.loading = true;
      if (res) {
        this.getCurrentLocation().then(() => {
          
          this.cobData.putActualizarGps(2, this.ordenes[0].IdPoliza, this.currentLatitude.toString(), this.currentLongitude.toString())
          .subscribe((rData) => {
            this.cobData.SyncCobradorPol(this.ordenes[0].IdCobrador).subscribe((data)=>{

            });
            this.reloadOrdenes();
            ons.notification.alert('Gps del cliente actualizado con la ubicación actual!', {title: ''});
            this.loading = false;
          });
        });
      } else {
        this.loading = false;
      }
    });
  }

  setNotAllCreditedStatus() {
    this.notAllCredited = false;
    this.ordenes.forEach((e) => {
      if (e.IdEstado !== '5') {
        this.notAllCredited = true;
      }
    });
  }

  validateAcreditar() {
    if (!this.flagAcreditar && !this.flagDesacreditar) { this.acreditar = 0; } // mensaje seleccione
    if (this.flagAcreditar && !this.flagDesacreditar) { this.acreditar = 1; } // acreditar
    if (!this.flagAcreditar && this.flagDesacreditar) { this.acreditar = 2; } // desacreditar
    if (this.flagAcreditar && this.flagDesacreditar) { this.acreditar = 3; } // seleccion mixta
  }

  setSelected(index: number, event: any, IdOrdenPago: number) {

    const checkState = event.target.checked;

    if (checkState) {

      this.selectedOrdenes.push(this.ordenes[index]);
      const currImporte = parseFloat(this.ordenes[index].Importe);
      this.totalImporte += currImporte;

      this.flagAcreditar = false;
      this.flagDesacreditar = false;
      for (const o of this.selectedOrdenes) {

        // console.log('check: ' + o.IdOrdenPago);
        if (o.EstadoOrden === '0') {
          this.flagAcreditar = true;
        } else if (o.EstadoOrden === '1') {
          this.flagDesacreditar = true;
        }
      }
      // console.log('acreditar: ' + this.flagAcreditar + ' desacreditar: ' + this.flagDesacreditar);
      this.validateAcreditar();

    } else {

      const nIndex = this.selectedOrdenes.map(key => key.IdOrdenPago).indexOf(IdOrdenPago);
      const currImporte = parseFloat(this.selectedOrdenes[nIndex].Importe);
      this.totalImporte -= currImporte;
      this.selectedOrdenes.splice(nIndex, 1);

      this.flagAcreditar = false;
      this.flagDesacreditar = false;
      for (const o of this.selectedOrdenes) {

        // console.log('check: ' + o.IdOrdenPago);
        if (o.EstadoOrden === '0') {
          this.flagAcreditar = true;

        } else if (o.EstadoOrden === '1') {
          this.flagDesacreditar = true;

        }
      }
      // console.log('acreditar: ' + this.flagAcreditar + ' desacreditar: ' + this.flagDesacreditar);
      this.validateAcreditar();

    }

  }

  acreditarOrdenes() {

    let sOrdenes = '';
    let message: string;
    let idOrdenesArray = [];
    let valor:number;

    for (const o of this.selectedOrdenes) {
      idOrdenesArray.push({ 'idOrden': o.IdOrdenPago });
      sOrdenes += o.IdOrdenPago + ' ';
    }

    
    const msg = `Desea actualizar la ubicación del cliente ${this.ordenes[0].Titular} con su ubicación actual?`;
    const confirmMsg = `La ubicación del cliente ${this.ordenes[0].Titular} se ha actualizado con exito!`;
    ons.notification.confirm(msg, {title: '', buttonLabels: ['No', 'Si']})
    .then((res) => {
      this.loading = true;
      if (res) {
        this.loading = true;
        this.cobData.putAcreditarOrden(idOrdenesArray, this.idPoliza, this.currentLatitude,
        this.currentLongitude, this.selectedOrdenes[0].LicenciaCliente).subscribe( (data) => {

          // console.log(data);
          

          if (this.selectedOrdenes.length === 0) {
            message = 'No ha seleccionado ninguna orden para acreditar.';
          } else if (this.selectedOrdenes.length === 1) {
            message = `La orden ${sOrdenes} ha sido acreditada`;
          } else if (this.selectedOrdenes.length > 1) {
            message = `Las ordenes:
            ${sOrdenes}
            han sido acreditadas`;
          }
          //this.reloadOrdenes();
          this.cobData.getOrdenesPolizaDetalle(this.idCobranza, this.idPoliza)
            .subscribe((data) => {
              this.ordenes = data;
              if (this.ordenes[0].IdRegistroAgenda) {
                this.IdRegistroAgenda = parseInt(this.ordenes[0].IdRegistroAgenda, 10);
              }
              this.domicilioLatitude = parseFloat(this.ordenes[0].LatitudDomicilio);
              this.domicilioLongitude = parseFloat(this.ordenes[0].LongitudDomicilio);
              
              this.getCurrentLocation().then(() => {
                const d = this.calculateUserDistanceWith(this.domicilioLatitude, this.domicilioLongitude);        
                if (d >= 0) {          
                  this.distancia = d.toString() + 'm';     
                  
                } else {
                  this.distancia = 'N/D';
                }
              });
              this.setNotAllCreditedStatus();
               ons.notification.alert(message, { title: '' }).then(() => {
                this.validarEliminarAgenda();
              });
            });
        
         
          this.cobData.SyncCobradorOrd(this.cobData.cobradorId).subscribe((data)=>{

          });

          
          this.totalImporte = 0;
          this.selectedOrdenes = [];
          this.flagAcreditar = false;
          this.flagDesacreditar = false;
          this.validateAcreditar();
          this.loading = false;
        });
        
      } else {
        valor=0;
        this.loading = true;
        this.cobData.putAcreditarOrden1(idOrdenesArray, this.idPoliza, this.selectedOrdenes[0].LicenciaCliente).subscribe( (data) => {

          // console.log(data);

          if (this.selectedOrdenes.length === 0) {
            message = 'No ha seleccionado ninguna orden para acreditar.';
          } else if (this.selectedOrdenes.length === 1) {
            message = `La orden ${sOrdenes} ha sido acreditada`;
          } else if (this.selectedOrdenes.length > 1) {
            message = `Las ordenes:
            ${sOrdenes}
            han sido acreditadas`;
          }
          this.cobData.SyncCobradorOrd(this.cobData.cobradorId).subscribe((data)=>{

          });

          this.reloadOrdenes();
          ons.notification.alert(message, { title: '' }).then(() => {
            this.validarEliminarAgenda();
          });
          this.totalImporte = 0;
          this.selectedOrdenes = [];
          this.flagAcreditar = false;
          this.flagDesacreditar = false;
          this.validateAcreditar();
          this.loading = false;
        });
       
      }
    });
    console.log('luis-maldonado');
  this.reloadOrdenes();
  }

  desacreditarOrdenes() {

    let sOrdenes = '';
    let message: string;
    let idOrdenesArray = [];

    for (const o of this.selectedOrdenes) {
      idOrdenesArray.push({ 'idOrden': o.IdOrdenPago });
      sOrdenes += o.IdOrdenPago + ' ';
    }

    this.loading = true;
    this.cobData.putDesacreditarOrden(idOrdenesArray, this.selectedOrdenes[0].LicenciaCliente)
    .subscribe( (data) => {
      // console.log(data);
      this.cobData.SyncCobradorOrd(this.cobData.cobradorId).subscribe((data)=>{

      });

      if (this.selectedOrdenes.length === 0) {
        message = 'No ha seleccionado ninguna orden para desacreditar.';
      } else if (this.selectedOrdenes.length === 1) {
        message = `La orden ${sOrdenes} ha sido desacreditada`;
      } else if (this.selectedOrdenes.length > 1) {
        message = `Las ordenes:
      ${sOrdenes}
      han sido desacreditadas`;
      }

      this.reloadOrdenes();
      ons.notification.alert(message, { title: '' });
      this.selectedOrdenes = [];
      this.flagAcreditar = false;
      this.flagDesacreditar = false;
      this.validateAcreditar();
      this.totalImporte = 0;
      this.loading = false;

    });

  }

  /* --------- Metodos Ruteables ------------------ */
  seeVisit() {
    this.cobData.IdRegistroAgenda = this.IdRegistroAgenda;
    this.cobData.cameFromOrdenDetalle = true;
    this.goTo('detalle-agenda-cobrador');
  }

  visitAgain() {
    this.cobData.cobForm_titular = this.ordenes[0].Titular;
    this.cobData.cobForm_direccion = this.ordenes[0].DomicilioCobro;
    this.cobData.cobForm_telefono = this.ordenes[0].Telefono;
    this.cobData.cameFromModificar = false;
    this.goTo('formulario-agenda-cobrador');
  }

  seeMap() {
    this.cobData.clienteFromAgenda = false;
    this.gps = {
      cLat: this.currentLatitude,
      cLon: this.currentLongitude,
      dLat: this.domicilioLatitude,
      dLon: this.domicilioLongitude,
      titular: this.ordenes[0].Titular,
      domicilio: this.ordenes[0].DomicilioCobro,
      telefono: this.ordenes[0].Telefono
    };
    this.cobData.gps = this.gps;
    this.goTo('clienteMap');
  }
  /* ------------------------------------------------*/

}
