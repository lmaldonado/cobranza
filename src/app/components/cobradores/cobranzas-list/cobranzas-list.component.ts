import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-cobranzas-list',
  templateUrl: './cobranzas-list.component.html',
  styleUrls: ['./cobranzas-list.component.scss']
})
export class CobranzasListComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/cobranzasList';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  cobranzas: any;

  ngOnInit() {

    this.validateSession();

    console.log(this.cobData.aRoute);
    this.cobData.getCobranzas(this.cobData.cobradorId, this.cobData.licenciaCliente)
      .subscribe(data => this.cobranzas = data);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  /* --------- Metodos Ruteables ------------------ */
  selectCobranza(id: number) {
    this.cobData.cobranzaId = id;
    this.goTo('ordenesList');
  }
  /* ------------------------------------------------*/

}
