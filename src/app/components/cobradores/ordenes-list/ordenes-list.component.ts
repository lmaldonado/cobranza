import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-ordenes-list',
  templateUrl: './ordenes-list.component.html',
  styleUrls: ['./ordenes-list.component.scss']
})
export class OrdenesListComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/ordenesList';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  cobranzaId: number;
  licenciaCliente: string;
  ordenes: any;
  noData: boolean;
  loading: boolean;

  // Geolocalización
  gpsTxt: any;
  gpsTxtLatitud: number;
  gpsTxtLongitud: number;
  gotCurrentLocation: boolean;

  ngOnInit() {

    this.validateSession();

    console.log(this.cobData.aRoute);
    this.cobranzaId = this.cobData.cobranzaId;
    this.licenciaCliente = this.cobData.licenciaCliente;
    try {
      this.cobData.getOrdenesPorPoliza(this.cobranzaId, this.licenciaCliente)
      .subscribe((data) => {
        const betaOrdenes = data;
        // this.ordenes = data;
        if (data.noData) {
          this.noData = true;
        } else {
          this.getCurrentLocation().then((resolvedData) => {
            // get distances
            let distance: string;
            betaOrdenes.forEach((e) => {
              distance = this.calculateUserDistanceWith(parseFloat(e.LatitudDomicilio), parseFloat(e.LongitudDomicilio)).toString();
              if (distance === '-1') {
                e.distance = 'N/D';
              } else {
                e.distance = distance + 'm';
              }
            });
            this.ordenes = data;
            // this.ordenes = this.sortByKey(betaOrdenes, 'distance');
            // console.log(this.ordenes);
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }

  sortByKey(array, key): any[] {
    return array.sort((a, b) => {
        const x = a[key]; const y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

  calculateUserDistanceWith(lat: number, lon: number): number {
    const dx = this.gpsTxtLatitud - lat;
    const dy = this.gpsTxtLongitud - lon;
    const res = parseFloat((Math.sqrt((Math.pow(dx, 2)) + (Math.pow(dy, 2))) * 100000).toFixed(0));
    if (isNaN(res)) {
      return -1;
    } else {
      return res;
    }
  }

  getCurrentLocation(): Promise<any> {
    const q = new Promise((resolve, reject) => {
      this.cobData.getGpsTxt().subscribe((data) => {
        this.gpsTxt = data;
        const commaIndex = this.gpsTxt.indexOf(',');
        const lat = this.gpsTxt.slice(0, commaIndex);
        const lon = this.gpsTxt.slice(commaIndex + 1);
        this.gpsTxtLatitud = parseFloat(lat);
        this.gpsTxtLongitud = parseFloat(lon);
        resolve(this.gotCurrentLocation = true);
      });
    });
    return q;
  }

  sortGps(): void {
    this.loading = true;
    this.getCurrentLocation().then((resolvedData) => {

      
      // get distances
      let distance: string;
      let distance2: any;
      this.ordenes.forEach((e) => {
        //distance = this.calculateUserDistanceWith(parseFloat(e.LatitudDomicilio), parseFloat(e.LongitudDomicilio)).toString();
        distance2 = this.calculateUserDistanceWith(parseFloat(e.LatitudDomicilio), parseFloat(e.LongitudDomicilio));
        if (distance2 === -1) {
          e.distance = 9999999999;
        } else {
          e.distance = distance2;
        }
        
      });
      console.log(this.ordenes);
      this.ordenes = this.sortByKey(this.ordenes, 'distance');
      
      this.ordenes.forEach((e) => {
        //distance = this.calculateUserDistanceWith(parseFloat(e.LatitudDomicilio), parseFloat(e.LongitudDomicilio)).toString();
        
        if (e.distance ===9999999999 ) {
           e.distance='N/D';
        }  else{
            e.distance= e.distance.toString() + 'm';
        }
        
      });
      this.loading = false;
      // console.log(this.clientes);
    });
  }

  sortEstado(): void {
    this.loading = true;
    this.ordenes = this.sortByKey(this.ordenes, 'EstadoOrdenes');
    this.loading = false;
  }

  /* --------- Metodos Ruteables ------------------ */
  verRendicion() {
    this.goTo('listado-rendiciones');
  }

  verUbicacionPendientes() {
    this.cobData.CobranzaOrdenes = this.ordenes;
    this.cobData.mapFromAgenda = false;
    this.goTo('domicilios-pendientes-map');
  }

  selectPoliza(polizaId: number) {
    this.cobData.polizaId = polizaId;
    this.cobData.cameFromOrdenesList = true;
    this.goTo('ordenesDetalle');
  }

  seeMap(orden: any) {
    const gps = {
      cLat: this.gpsTxtLatitud,
      cLon: this.gpsTxtLongitud,
      dLat: parseFloat(orden.LatitudDomicilio),
      dLon: parseFloat(orden.LongitudDomicilio),
      domicilio: orden.Domicilio,
      telefono: orden.Telefono,
      titular: orden.Titular
    };
    this.cobData.gps = gps;
    this.goTo('clienteMap');
  }
  /* ------------------------------------------------*/

}
