import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CobAgendaDetalleComponent } from './cob-agenda-detalle.component';

describe('CobAgendaDetalleComponent', () => {
  let component: CobAgendaDetalleComponent;
  let fixture: ComponentFixture<CobAgendaDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobAgendaDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CobAgendaDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
