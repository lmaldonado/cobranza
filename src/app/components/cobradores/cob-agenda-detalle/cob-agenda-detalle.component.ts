import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, DoCheck, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { LoadingDarkOverlayComponent } from '../../../helper-directives/loading-dark-overlay/loading-dark-overlay.component';

@Component({
  selector: 'app-cob-agenda-detalle',
  templateUrl: './cob-agenda-detalle.component.html',
  styleUrls: ['./cob-agenda-detalle.component.scss']
})
export class CobAgendaDetalleComponent implements OnInit, OnDestroy {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/detalle-agenda-cobrador';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  cliente: any;
  loading = false;
  gpsTxt: any;
  gpsTxtLatitud: number;
  gpsTxtLongitud: number;
  gotCurrentLocation = false;
  clienteLatitud: number;
  clienteLongitud: number;
  gps: any;
  LicenciaCliente: string;
  idRegistroAgenda: number;
  cameFromOrdenDetalle = false;

  ngOnInit() {

    this.validateSession();

    console.log(this.cobData.aRoute);
    this.idRegistroAgenda = this.cobData.IdRegistroAgenda;
    this.loading = true;
    this.cobData.getClientesAgendaDetalle(this.idRegistroAgenda).subscribe((data) => {
      this.cliente = data[0];
      // console.log(this.cliente);
      this.cameFromOrdenDetalle = this.cobData.cameFromOrdenDetalle ? true : false;
      this.clienteLatitud = parseFloat(this.cliente.LatitudDomicilio);
      this.clienteLongitud = parseFloat(this.cliente.LongitudDomicilio);
      this.getCurrentLocation();
      this.LicenciaCliente = this.cobData.licenciaCliente;
      this.loading = false;
    });
    // this.cliente = this.cobData.clienteSeleccionado;
  }

  ngOnDestroy() {
    this.cobData.cameFromOrdenDetalle = false;
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
    if (this.cobData.tipoUsuario !== 'cobrador') {
      this.router.navigate(['']);
    }
  }

  actualizarGPS() {
    const msg = `Esta seguro que desea actualizar la ubicación del cliente ${this.cliente.NombreCliente} con su ubicación actual?`;
    const confirmMsg = `La ubicación del cliente ${this.cliente.NombreCliente} se ha actualizado con exito!`;
    ons.notification.confirm(msg, { title: '', buttonLabels: ['No', 'Si'] })
    .then((res) => {
      if (res) {
        this.loading = true;
        const lat = this.gpsTxtLatitud.toString();
        const lon = this.gpsTxtLongitud.toString();
        this.cobData.putActualizarGps(2, this.cliente.IdPoliza, lat, lon)
        .subscribe((data) => {
          const updated = data[0];
          this.cliente.LatitudDomicilio = updated.LatitudDomicilio;
          this.cliente.LongitudDomicilio = updated.LongitudDomicilio;
          this.clienteLatitud = parseFloat(this.cliente.LatitudDomicilio);
          this.clienteLongitud = parseFloat(this.cliente.LongitudDomicilio);
          this.loading = false;

          ons.notification.alert(confirmMsg, { title: '' });
          this.cobData.SyncCobradorPol(this.cobData.cobradorId).subscribe((data)=>{

          });
        });
      } else {
        this.loading = false;
      }
    });
  }

  getCurrentLocation() {
    this.cobData.getGpsTxt().subscribe((data) => {
      this.gpsTxt = data;
      const commaIndex = this.gpsTxt.indexOf(',');
      const lat = this.gpsTxt.slice(0, commaIndex);
      const lon = this.gpsTxt.slice(commaIndex + 1);
      this.gpsTxtLatitud = parseFloat(lat);
      this.gpsTxtLongitud = parseFloat(lon);
      this.gotCurrentLocation = true;
    });
  }

  getCurrentLocationNEW(): Promise<any> {
    let gpsTxt: string;
      const q = new Promise((resolve, reject) => {
      this.cobData.getGpsTxt().subscribe((data) => {
        gpsTxt = data;
        const commaIndex = gpsTxt.indexOf(',');
        const lat = gpsTxt.slice(0, commaIndex);
        const lon = gpsTxt.slice(commaIndex + 1);
        this.gpsTxtLatitud = parseFloat(lat);
        this.gpsTxtLongitud = parseFloat(lon);
        resolve({ gotCurrentLocation: true});
      });
    });
    return q;
  }

  /* --------- Metodos Ruteables ------------------ */
  verCuentaCliente() {
    this.cobData.cobranzaId = this.cliente.IdCobranza;
    this.cobData.polizaId = this.cliente.IdPoliza;
    this.cobData.cameFromAgenda = true;
    this.goTo('ordenesDetalle');
  }

  modificarAgendado() {
    this.cobData.clienteSeleccionado = this.cliente;
    this.cobData.cameFromModificar = true;
    this.goTo('formulario-agenda-cobrador');
  }

  deleteCliente() {
    const msg = `Esta seguro que desea eliminar de la agenda al cliente ${this.cliente.NombreCliente}?`;
    ons.notification.confirm(msg, { title: '', buttonLabels: ['Cancelar', 'Eliminar'] })
      .then((res) => {
        if (res) {
          this.loading = true;
          this.cobData.deleteClienteAgenda(this.cliente.IdRegistro).toPromise()
            .then((data) => {
              ons.notification.alert(`Cliente ${this.cliente.NombreCliente} Eliminado`, { title: '' });
              this.cliente = '';
              this.loading = false;
              this.goBack();
            });
        } else {
          this.loading = false;
        }
      });
  }

  seeMap() {
    this.cobData.clienteFromAgenda = true;
    this.gps = {
      cLat: this.gpsTxtLatitud,
      cLon: this.gpsTxtLongitud,
      dLat: this.clienteLatitud,
      dLon: this.clienteLongitud,
      titular: this.cliente.NombreCliente,
      telefono: this.cliente.Telefono,
      domicilio: this.cliente.Direccion
    };
    this.cobData.gps = this.gps;
    this.goTo('clienteMap');
  }
  /* ------------------------------------------------*/

}
