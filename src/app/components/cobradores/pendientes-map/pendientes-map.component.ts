import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-pendientes-map',
  templateUrl: './pendientes-map.component.html',
  styleUrls: ['./pendientes-map.component.scss']
})
export class PendientesMapComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/domicilios-pendientes-map';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  ordenes: any;
  agendados: any;
  cameFromAgenda: boolean;
  domicilios: any[] = [];
  gpsTxt: any;
  currentLatitude: number;
  currentLongitude: number;
  currentTitular: string;
  currentDomicilio: string;
  currentTelefono: string;
  lastId = -1; // handler para el doble click ver detalle en verDatos()
  markers = {
    red: '../../../../assets/maps-markers/marker-red.png',
    redDot: '../../../../assets/maps-markers/marker-red-dot.png',
    redSm: '../../../../assets/maps-markers/marker-red-sm.png',
    redDotSm: '../../../../assets/maps-markers/marker-red-dot-sm.png',
    yellow: '../../../../assets/maps-markers/marker-yellow.png',
    yellowDot: '../../../../assets/maps-markers/marker-yellow-dot.png',
    yellowSm: '../../../../assets/maps-markers/marker-yellow-sm.png',
    yellowDotSm: '../../../../assets/maps-markers/marker-yellow-dot-sm.png',
    green: '../../../../assets/maps-markers/marker-green.png',
    greenDot: '../../../../assets/maps-markers/marker-green-dot.png',
    greenSm: '../../../../assets/maps-markers/marker-green-sm.png',
    greenDotSm: '../../../../assets/maps-markers/marker-green-dot-sm.png',
    blue: '../../../../assets/maps-markers/marker-blue.png',
    blueDot: '../../../../assets/maps-markers/marker-blue-dot.png',
    blueSm: '../../../../assets/maps-markers/marker-blue-sm.png',
    blueDotSm: '../../../../assets/maps-markers/marker-blue-dot-sm.png',
    gray: '../../../../assets/maps-markers/marker-gray.png',
    grayDot: '../../../../assets/maps-markers/marker-gray-dot.png',
    graySm: '../../../../assets/maps-markers/marker-gray-sm.png',
    grayDotSm: '../../../../assets/maps-markers/marker-gray-dot-sm.png',
    darkgray: '../../../../assets/maps-markers/marker-darkgray.png',
    darkgrayDot: '../../../../assets/maps-markers/marker-darkgray-dot.png',
    darkgraySm: '../../../../assets/maps-markers/marker-darkgray-sm.png',
    darkgrayDotSm: '../../../../assets/maps-markers/marker-darkgray-dot-sm.png',
    orange: '../../../../assets/maps-markers/marker-orange.png',
    orangeDot: '../../../../assets/maps-markers/marker-orange-dot.png',
    orangeSm: '../../../../assets/maps-markers/marker-orange-sm.png',
    orangeDotSm: '../../../../assets/maps-markers/marker-orange-dot-sm.png'
  };

  ngOnInit() {

    this.validateSession();

    console.log(this.cobData.aRoute);
    this.cameFromAgenda = this.cobData.mapFromAgenda;
    this.getCurrentLocation();
    if (this.cameFromAgenda) {
      this.agendados = this.cobData.clientesAgenda;
      this.setDomiciliosData(this.agendados);
    } else {
      this.ordenes = this.cobData.CobranzaOrdenes;
      this.setDomiciliosData(this.ordenes);
    }
    this.currentTitular = 'Visitas Pendientes';
    this.currentDomicilio = '';
    this.currentTelefono = '';
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }

  getCurrentLocation() {

    this.cobData.getGpsTxt().subscribe((data) => {
      this.gpsTxt = data;
      const commaIndex = this.gpsTxt.indexOf(',');
      const lat = this.gpsTxt.slice(0, commaIndex);
      const lon = this.gpsTxt.slice(commaIndex + 1);
      this.currentLatitude = parseFloat(lat);
      this.currentLongitude = parseFloat(lon);
      // console.log(`lat: ${lat} / lon: ${lon}`);
    });
  }

  setDomiciliosData(arrayData: any[]) {

    // console.log(arrayData);
    let parsedLat: number;
    let parsedLon: number;

    if (this.cameFromAgenda) {
      arrayData.forEach((e) => {

        parsedLat = parseFloat(e.LatitudDomicilio);
        parsedLon = parseFloat(e.LongitudDomicilio);

        if (parseInt(e.DiferenciaFecha, 10) === 0 ) {
          this.domicilios.push({
            idPoliza: e.IdPoliza,
            idCobranza: e.IdCobranza,
            lat: parsedLat,
            lon: parsedLon,
            color: this.markers.greenSm,
            titular: e.NombreCliente,
            telefono: e.Telefono,
            domicilio: e.Direccion });
        } else if (parseInt(e.DiferenciaFecha, 10) === 1) {
          this.domicilios.push({
            idPoliza: e.IdPoliza,
            idCobranza: e.IdCobranza,
            lat: parsedLat,
            lon: parsedLon,
            color: this.markers.yellowSm,
            titular: e.NombreCliente,
            telefono: e.Telefono,
            domicilio: e.Direccion });
        } else if (parseInt(e.DiferenciaFecha, 10) >= 2) {
          this.domicilios.push({
            idPoliza: e.IdPoliza,
            idCobranza: e.IdCobranza,
            lat: parsedLat,
            lon: parsedLon,
            color: this.markers.redSm,
            titular: e.NombreCliente,
            telefono: e.Telefono,
            domicilio: e.Direccion });
        } else {
          this.domicilios.push({
            idPoliza: e.IdPoliza,
            idCobranza: e.IdCobranza,
            lat: parsedLat,
            lon: parsedLon,
            color: this.markers.darkgraySm,
            titular: e.NombreCliente,
            telefono: e.Telefono,
            domicilio: e.Direccion });
        }

      });
    } else {
      arrayData.forEach((e) => {

        parsedLat = parseFloat(e.LatitudDomicilio);
        parsedLon = parseFloat(e.LongitudDomicilio);

        switch (e.EstadoOrdenes) {
          case '0':
            this.domicilios.push({
              idPoliza: e.IdPoliza,
              idCobranza: e.IdCobranza,
              lat: parsedLat,
              lon: parsedLon,
              color: this.markers.darkgraySm,
              titular: e.Titular,
              telefono: e.Telefono,
              domicilio: e.Domicilio });
            break;
          case '1':
            this.domicilios.push({
              idPoliza: e.IdPoliza,
              idCobranza: e.IdCobranza,
              lat: parsedLat,
              lon: parsedLon,
              color: this.markers.redSm,
              titular: e.Titular,
              telefono: e.Telefono,
              domicilio: e.Domicilio });
            break;
        }

      });
    }
    // console.log(this.domicilios);
  }

  /* --------- Metodos Ruteables ------------------ */
  verDatos(domicilio: any) {
    if (domicilio.idPoliza === this.lastId) {
      this.cobData.cobranzaId = domicilio.idCobranza;
      this.cobData.polizaId = domicilio.idPoliza;
      this.cobData.cameFromPendientesMap = true;
      this.goTo('ordenesDetalle');
    } else {
      this.lastId = domicilio.idPoliza;
      this.currentTitular = domicilio.titular;
      this.currentTelefono = `Tlf. ${domicilio.telefono}`;
      this.currentDomicilio = domicilio.domicilio;
    }
  }
  /* ------------------------------------------------*/

}
