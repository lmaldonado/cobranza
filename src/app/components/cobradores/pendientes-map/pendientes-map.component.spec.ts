import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendientesMapComponent } from './pendientes-map.component';

describe('PendientesMapComponent', () => {
  let component: PendientesMapComponent;
  let fixture: ComponentFixture<PendientesMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendientesMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendientesMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
