import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteMapComponent } from './cliente-map.component';

describe('ClienteMapComponent', () => {
  let component: ClienteMapComponent;
  let fixture: ComponentFixture<ClienteMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
