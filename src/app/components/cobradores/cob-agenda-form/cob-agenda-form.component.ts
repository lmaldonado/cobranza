import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, DoCheck, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { LoadingDarkOverlayComponent } from '../../../helper-directives/loading-dark-overlay/loading-dark-overlay.component';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-cob-agenda-form',
  templateUrl: './cob-agenda-form.component.html',
  styleUrls: ['./cob-agenda-form.component.scss']
})
export class CobAgendaFormComponent implements OnInit, OnDestroy, DoCheck {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/formulario-agenda-cobrador';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;

  myDatePickerOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    dayLabels: { su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mie', th: 'Jue', fr: 'Vie', sa: 'Sab' },
    monthLabels: {
      1: 'Ene', 2: 'Feb', 3: 'Mar', 4: 'Abr', 5: 'May', 6: 'Jun',
      7: 'Jul', 8: 'Ago', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dic'
    },
    todayBtnTxt: 'Hoy'
  };
  // Geolocalización
  gpsTxt: any;
  gpsTxtLatitud: number;
  gpsTxtLongitud: number;
  gotCurrentLocation: boolean;

  nuevoRegistro: any;
  idCobranza: number;
  idPoliza: number;
  loading = false;
  fechaPlaceholder: string;

  // Param Properties
  paramNombre: string;
  paramTelefono: string;
  paramDireccion: string;
  paramFechaHora: string;
  paramObservaciones: string;

  // Form Properties
  formNombre: string;
  formTelefono: string;
  formDireccion: string;
  formFecha: any;
  formTiempo = '';
  formHora = '0';
  formMinuto = '0';
  formObservaciones: string;

  // fecha
  dia: string;
  mes: string;
  ano: string;

  cameFromModificar = false;
  selectedCliente: any;

  ngOnInit() {

    this.validateSession();
    this.getCurrentLocation();

    console.log(this.cobData.aRoute);
    this.formNombre = this.cobData.cobForm_titular;
    this.formDireccion = this.cobData.cobForm_direccion;
    this.formTelefono = this.cobData.cobForm_telefono;
    this.cameFromModificar = this.cobData.cameFromModificar;

    if (this.cameFromModificar) {
      this.selectedCliente = this.cobData.clienteSeleccionado;
      this.formNombre = this.selectedCliente.NombreCliente;
      this.formTelefono = this.selectedCliente.Telefono;
      this.formDireccion = this.selectedCliente.Direccion;
      if (this.selectedCliente.FechaVisita) {
        this.formFecha = { date: { year: this.selectedCliente.Ano, month: this.selectedCliente.Mes, day: this.selectedCliente.Dia } };
      } else {
        this.fechaPlaceholder = `Fecha de visita...`;
      }
      this.formHora = this.selectedCliente.Hora;
      this.formMinuto = this.selectedCliente.Minutos;
      this.formTiempo = `${this.formHora}:${this.formMinuto}`;
      this.formObservaciones = this.selectedCliente.Observacion;
    }

    this.fechaPlaceholder = this.formFecha ? this.formFecha : 'Seleccione una fecha';
    this.idCobranza = this.cobData.cobranzaId;
    this.idPoliza = this.cobData.polizaId;
  }

  ngOnDestroy() {
    this.cameFromModificar = false;
  }

  ngDoCheck() {
    this.initiateMasks();
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  onDateChanged(dateEvent: IMyDateModel): void {
    this.formFecha = dateEvent.formatted;
  }

  initiateMasks() {
    let charLen: string;

    charLen = ''; for (let i = 0; i <= 25; i++) { charLen = charLen + 'S'; }
    $('#formNombre').mask(charLen, {placeholder: 'Nombre...', 'translation': {S: {pattern: /[^,;/\\\\*%&()'"]+/}}});

    $('#formdp').mask('00-00-0000', {placeholder: '__-__-____'});
    $('#formTelefono').mask('000-0000-0000000000', {placeholder: '000-0000-0000'});
    $('#formTiempo').mask('H0:M0', {placeholder: 'HH:MM', 'translation': {H: {pattern: /^[0-2]$/}, M: {pattern: /^[0-5]$/}}});

  }

  validateForm() {

    const REG_EX = /[,;/\\\\*%&()'"]+/g;

    if (!this.formNombre) {
      ons.notification.alert('Debe Ingresar un nombre de cliente', {title: ''});
      return false;
    }

    if (!this.formTelefono) {
      this.formTelefono = null;
    }

    if (!this.formDireccion) {
      ons.notification.alert('Debe Ingresar una dirección', {title: ''});
      return false;
    }

    if (!this.formFecha) {
      ons.notification.alert('Debe Ingresar una fecha de visita valida', {title: ''});
      return false;
    }

    /*if (this.formHora === '0' || this.formMinuto === '0') {
      ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
      return false;
    }*/

    if (this.formTiempo.length !== 5) {
      ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
      this.formTiempo = '';
      return false;
    } else if (this.formTiempo.length === 5) {
      const h = parseFloat(this.formTiempo.substr(0, 2));
      const m = parseFloat(this.formTiempo.substr(3, 4));
      if (h > 23 || m > 59) {
        ons.notification.alert('Debe Ingresar una hora de visita valida', {title: ''});
        this.formTiempo = '';
        return false;
      } else {
        this.formHora = this.formTiempo.substr(0, 2);
        this.formMinuto = this.formTiempo.substr(3, 4);
      }
    }

    if (!this.formObservaciones) {
      this.formObservaciones = null;
    } else {
      this.paramObservaciones = this.formObservaciones.replace(REG_EX, ' ');
    }

    this.paramNombre = this.formNombre;
    this.paramTelefono = this.formTelefono;
    this.paramFechaHora = this.formFecha.formatted ? this.formFecha.formatted : this.formFecha;
    this.paramFechaHora = `${this.paramFechaHora} ${this.formHora}:${this.formMinuto}:00`;
    this.paramDireccion = this.formDireccion.replace(REG_EX, ' ');
    if (this.formFecha) {
      this.dia = this.formFecha.date.day >= 10 ? `${this.formFecha.date.day}` : `0${this.formFecha.date.day}`;
      this.mes = this.formFecha.date.month >= 10 ? `${this.formFecha.date.month}` : `0${this.formFecha.date.month}`;
      this.ano = this.formFecha.date.year.toString();
      this.paramFechaHora = `${this.ano}-${this.mes}-${this.dia} ${this.formHora}:${this.formMinuto}:00`;
    }

    return true;
  }

  /* --------- Metodos Ruteables ------------------ */
  modificarCliente() {
    this.loading = true;
    if (this.validateForm()) {
      const paramsArray = [{
        idRegistro: this.selectedCliente.IdRegistro,
        idEstadoVisita: null,
        nombre: this.paramNombre,
        telefono: this.paramTelefono,
        direccion: this.paramDireccion,
        fechahora: this.paramFechaHora,
        observaciones: this.paramObservaciones,
        latitud: this.gpsTxtLatitud.toString(),
        longitud: this.gpsTxtLongitud.toString(),
        dni: null,
        contrato: null,
        importe: null,
        fechaNacimiento: null
      }];
      this.cobData.putCobranzaModificarCliente(paramsArray).subscribe((data) => {
        this.nuevoRegistro = data[0];
        ons.notification.alert(`Se Modifico la información del cliente con exito!`, {title: ''});
        this.loading = false;
        this.goBack();
      });
    } else {
      this.loading = false;
    }

  }

  agendarCliente() {

    this.loading = true;
    if (this.validateForm()) {
      const paramsArray = [{
        idUsuario: this.cobData.loggedUserId.toString(),
        nombre: this.paramNombre,
        telefono: this.paramTelefono,
        direccion: this.paramDireccion,
        fechahora: this.paramFechaHora,
        observaciones: this.paramObservaciones,
        idCobranza: this.idCobranza,
        idPoliza: this.idPoliza,
        latitud: this.gpsTxtLatitud.toString(),
        longitud: this.gpsTxtLongitud.toString(),
        idEstadoVisita: null,
        dni: null,
        contrato: null,
        importe: null,
        fechaNacimiento: null
      }];
      // console.log(paramsArray);
      this.cobData.postClienteAgenda(paramsArray)
        .subscribe((data) => {
          this.nuevoRegistro = data[0];
          ons.notification.alert(`Se agendo la nueva visita para ${this.nuevoRegistro.NombreCliente}`, {title: ''});
          this.loading = false;
          this.goBack();
        });
    } else {
      this.loading = false;
    }
  }
   getCurrentLocation() {
    this.cobData.getGpsTxt().subscribe((data) => {
      this.gpsTxt = data;
      const commaIndex = this.gpsTxt.indexOf(',');
      const lat = this.gpsTxt.slice(0, commaIndex);
      const lon = this.gpsTxt.slice(commaIndex + 1);
      this.gpsTxtLatitud = parseFloat(lat);
      this.gpsTxtLongitud = parseFloat(lon);
      this.gotCurrentLocation = true;
    });
  }
  /* ------------------------------------------------*/

}
