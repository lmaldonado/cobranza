import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CobAgendaFormComponent } from './cob-agenda-form.component';

describe('CobAgendaFormComponent', () => {
  let component: CobAgendaFormComponent;
  let fixture: ComponentFixture<CobAgendaFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobAgendaFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CobAgendaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
