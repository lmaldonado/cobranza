import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCobradoresComponent } from './main-cobradores.component';

describe('MainCobradoresComponent', () => {
  let component: MainCobradoresComponent;
  let fixture: ComponentFixture<MainCobradoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainCobradoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCobradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
