import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  OnInit
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { Router } from '@angular/router';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';

@Component({
  selector: 'app-main-cobradores',
  templateUrl: './main-cobradores.component.html',
  styleUrls: ['./main-cobradores.component.scss']
})
export class MainCobradoresComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/mainc';
  appLogo = '../../../../assets/appLogo.png';
  cobrador: any;
  idCobrador: any;
  nombreCobrador: string;
  comisionCobrador: number;
  documentoCobrador: string;
  menuCollapsed = true;

  ngOnInit() {

    this.validateSession();

    console.log(this.cobData.aRoute);
    this.idCobrador = this.cobData.cobradorId;
    this.nombreCobrador = this.cobData.usuarioNombre;
    this.comisionCobrador = this.cobData.cobradorComision;
    this.documentoCobrador = this.cobData.usuarioDocumento;

  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  cerrarSesion(): void {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

   /* --------- Metodos Ruteables ------------------ */
  openCobranzas(): void {
    this.goTo('cobranzasList');
  }

  openAgenda(): void {
    this.goTo('listado-agenda-cobrador');
  }
   sync(): void {
    this.goTo('Sync');
  }
   ruta(): void {
    this.cobData.ConsultaRuta().subscribe((data)=>{
      let gpsArray = [];
      data.forEach( (e) => {
        console.log(e);
        gpsArray.push({
          latitud: parseFloat(e.Latitud),
          longitud: parseFloat(e.Longitud),
          fecha: e.fecha,
          hora: e.hora,

        });
      });
      this.cobData.rutas=gpsArray;
       this.goTo('Ruta');
    });
   
  }
   /* ---------------------------------------------- */

}
