import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-rendicion-detalle',
  templateUrl: './rendicion-detalle.component.html',
  styleUrls: ['./rendicion-detalle.component.scss']
})
export class RendicionDetalleComponent implements OnInit, OnDestroy {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/detalle-rendicion';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  idCobranza: number;
  idCobrador: number;
  licenciaCliente: string;
  rendiciones: any;
  totalImporte = 0;
  totalComision = 0;
  noData = false;
  flagAcreditadas: boolean;
  flagNoAcreditadas: boolean;
  flagRendicion: boolean;
  pageSubtitle: string;
  noDataMessage: string;
  idRendicion: number;

  ngOnInit() {

    this.validateSession();

    console.log(this.cobData.aRoute);
    this.idCobranza = this.cobData.cobranzaId;
    this.licenciaCliente = this.cobData.licenciaCliente;
    this.flagAcreditadas = this.cobData.flagAcreditadas;
    this.flagNoAcreditadas = this.cobData.flagNoAcreditadas;
    this.flagRendicion = this.cobData.flagRendicion;
    this.idRendicion = this.cobData.idRendicion;

    if (this.flagAcreditadas) {
      this.pageSubtitle = `Ordenes Acreditadas de la cobranza #${this.idCobranza}`;
      this.cobData.getRendicionOrdenesAcreditadas(this.idCobranza, this.licenciaCliente)
      .subscribe((data) => {
        console.log(data);
        if (data.error){
          this.noData = true;
          this.noDataMessage = `No existen ordenes acreditadas actualmente.`;
        } else {
          this.rendiciones = data;
          this.calcularTotales();
        }
      });
    }

    if (this.flagNoAcreditadas) {
      this.pageSubtitle = `Ordenes No Acreditadas de la cobranza #${this.idCobranza}`;
      this.cobData.getRendicionOrdenesNoAcreditadas(this.idCobranza, this.licenciaCliente)
      .subscribe((data) => {
        console.log(data);
        if (data.error) {
          this.noData = true;
          this.noDataMessage = `No existen ordenes no acreditadas actualmente.`;
        } else {
          this.rendiciones = data;
          this.calcularTotales();
        }
      });
    }

    if (this.flagRendicion) {
      this.pageSubtitle = `Ordenes rendidas de la Rendicion #${this.idRendicion}`;
      this.cobData.getRendicionDetalle(this.idRendicion, this.licenciaCliente)
        .subscribe((data) => {
          console.log(data);
          if (data.error) {
            this.noData = true;
            this.noDataMessage = `Error inesperado!.`;
          } else {
            this.rendiciones = data;
            this.calcularTotales();
          }
        });
    }

  }

  ngOnDestroy() {
    this.cobData.flagAcreditadas = undefined;
    this.cobData.flagNoAcreditadas = undefined;
    this.cobData.flagRendicion = undefined;
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  calcularTotales() {
    this.rendiciones.forEach((e) => {
      this.totalImporte += parseFloat(e.Importe);
      this.totalComision += parseFloat(e.Comision);
    });
  }

  /* --------- Metodos Ruteables ------------------ */
  /* ------------------------------------------------*/

}
