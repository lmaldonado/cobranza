import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CobAgendaListComponent } from './cob-agenda-list.component';

describe('CobAgendaListComponent', () => {
  let component: CobAgendaListComponent;
  let fixture: ComponentFixture<CobAgendaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobAgendaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CobAgendaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
