import {
  Component,
  ViewChild,
  OnsTab,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { NgIf } from '@angular/common';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../../../services/cobdata.service';
import { Router } from '@angular/router';
import * as ons from 'onsenui';
import { OnInit, OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-cob-agenda-list',
  templateUrl: './cob-agenda-list.component.html',
  styleUrls: ['./cob-agenda-list.component.scss']
})
export class CobAgendaListComponent implements OnInit {

  constructor(private cobData: CobdataService, private router: Router) { }

  componentRoute = '/listado-agenda-cobrador';
  homeComponentRoute = '/mainc';
  menuCollapsed = true;
  clientes: any;
  noData = false;

  ngOnInit() {

    this.validateSession();

    console.log(this.cobData.aRoute);
    this.cobData.getClientesAgenda().toPromise().then((data) => {
      this.clientes = data;
      if (this.clientes.noData) {
        this.noData = true;
      }
      // console.log(this.clientes);
    });

  }

  validateSession(): void {
    if (!this.cobData.loggedUserId) {
      this.router.navigate(['']);
    }
    if (this.cobData.tipoUsuario !== 'cobrador') {
      this.router.navigate(['']);
    }
  }

  cerrarSesion() {
    this.cobData.clearSessionVars();
    this.router.navigate(['']);
  }

  goTo(route: string): void {
    this.cobData.aRoute.push(this.componentRoute);
    this.router.navigate([route]);
  }

  goHome(): void {
    this.cobData.aRoute = [];
    this.router.navigate([this.homeComponentRoute]);
  }

  goBack(): void {
    const backRoute = this.cobData.aRoute.pop();
    this.router.navigate([backRoute]);
  }

  /* --------- Metodos Ruteables ------------------ */
  verUbicacionAgendados() {
    this.cobData.mapFromAgenda = true;
    this.cobData.clientesAgenda = this.clientes;
    this.goTo('domicilios-pendientes-map');
  }

  selectCliente(idRegistroAgenda: number) {
    this.cobData.IdRegistroAgenda = idRegistroAgenda;
    this.goTo('detalle-agenda-cobrador');
  }
  /* ------------------------------------------------*/

}
