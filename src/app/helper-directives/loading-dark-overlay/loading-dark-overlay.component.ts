import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-dark-overlay',
  templateUrl: './loading-dark-overlay.component.html',
  styleUrls: ['./loading-dark-overlay.component.scss']
})
export class LoadingDarkOverlayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
