import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingDarkOverlayComponent } from './loading-dark-overlay.component';

describe('LoadingDarkOverlayComponent', () => {
  let component: LoadingDarkOverlayComponent;
  let fixture: ComponentFixture<LoadingDarkOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingDarkOverlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingDarkOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
