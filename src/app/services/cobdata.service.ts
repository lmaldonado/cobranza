import { Injectable } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Observable } from 'rxjs/Observable';

import { HttpClient } from '@angular/common/http';
import { Session } from '../interfaces/app-interfaces';


@Injectable()
export class CobdataService {

  constructor(public http: HttpClient) { }

  // BACKEND SERVERS
  private SERVERS = {
    LOCAL: {
      HOST: 'http://localhost:8080',
      BACKEND_FOLDER: 'cobranzasWebAPI/mainApp.php'
    },
    REMOTE: {
      //HOST: 'http://cob.neosepelios.com.ar',
      HOST: 'http://localhost:8080',
      BACKEND_FOLDER: 'cobranzasWebAPI/mainApp.php'
    }
  };
  

  // Session vars
  public session: Session;
  public loggedUserId: number;
  public tipoUsuario: string;
  public cobradorId: number;
  public cobradorComision: number;
  public productorId: number;
  public usuarioNombre: string;
  public usuarioDocumento: string;
  public licenciaCliente: string;
  public licenciafg:string;
  public cobranzaId: number;
  public polizaId: number;
  public aRoute = []; // array de ruta

  // utiliti vars
  public clienteSeleccionado: any;
  public gps: any;
  public latitud:any;
  public longitud:any;
  public rutas:any;
  public CobranzaOrdenes: any;
  public mapFromAgenda: boolean;
  public clienteFromAgenda: boolean;
  public clientesAgenda: any;
  public estadoInicial: number; // estado seleccionado del formulario de productores
  public goBack: string; // Variable que define la ruta anterior para el boton "Atras"

  // cob-agenda
  public cobForm_titular;
  public cobForm_telefono;
  public cobForm_direccion;
  public cameFromAgenda: boolean;
  public IdRegistroAgenda: number;
  public cameFromOrdenDetalle: boolean;
  public cameFromModificar: boolean;

  // cameFrom vars
  public cameFromPendientesMap: boolean;
  public cameFromOrdenesList: boolean;
  public cameFromMain: boolean;

  // productor agenda map
  public gpsAgendados: any;
  public flagPendientesMap: boolean;
  public flagClienteMap: boolean;

  // rendiciones
  public flagAcreditadas: boolean;
  public flagNoAcreditadas: boolean;
  public flagRendicion: boolean;
  public idRendicion: number;
  // Variables Instalacion Inicial
  public cobradorToSync: any;
  

  clearSessionVars() {
    this.latitud = undefined;
    this.longitud = undefined;
    this.loggedUserId = undefined;
    this.tipoUsuario = undefined;
    this.cobradorId = undefined;
    this.productorId = undefined;
    this.usuarioNombre = undefined;
    this.usuarioDocumento = undefined;
    this.licenciaCliente = undefined;
    this.cobranzaId = undefined;
    this.polizaId = undefined;
    this.cobradorComision = undefined;
    this.gps = undefined;
    this.rutas=undefined;
    this.cobForm_titular = undefined;
    this.cobForm_direccion = undefined;
    this.cobForm_telefono = undefined;
    this.cameFromAgenda = undefined;
    this.CobranzaOrdenes = undefined;
    this.mapFromAgenda = undefined;
    this.flagAcreditadas = undefined;
    this.flagNoAcreditadas = undefined;
    this.flagRendicion = undefined;
    this.idRendicion = undefined;
    this.cameFromOrdenDetalle = undefined;
    this.cameFromModificar = undefined;
    this.clienteSeleccionado = undefined;
    this.cameFromPendientesMap = undefined;
    this.cobradorToSync = undefined;
    this.licenciafg=undefined;
    this.aRoute = [];
  }

  // Servicios de instalacion inicial
  
  Innstalacion(): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
   // const ROUTE = 'wizard01';
    const ROUTE = 'Instalacion';

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE;
    return this.http.get(url);
  }
  writeINI(nomdb:string,user:string): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
   // const ROUTE = 'wizard01';
   const PARAMS = `${nomdb}/${user}`;
    const ROUTE = 'writeINI';

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE+'/'+PARAMS;
    return this.http.get(url);
  }
  licencia(licensekey:string): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
   // const ROUTE = 'wizard01';
   const PARAMS = `${licensekey}`;
    const ROUTE = 'licencia';

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE+'/'+PARAMS;
    return this.http.get(url);
  }

  wizard01(): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
   // const ROUTE = 'wizard01';
    const ROUTE = 'createCobWebData';

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE;
    return this.http.get(url);
  }
  wizard02(): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
   // const ROUTE = 'wizard01';
    const ROUTE = 'funcionesCobWebData';

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE;
    return this.http.get(url);
  }
   cobradoresRemoto(dni:string): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
    const PARAMS = `${dni}`;
    const ROUTE = 'cobradoresRemoto';

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE +'/'+PARAMS;
    return this.http.get(url);
  }
  productoresRemoto(dni:string): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
    const PARAMS = `${dni}`;
    const ROUTE = 'productoresRemoto';

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE +'/'+PARAMS;
    return this.http.get(url);
  }
  ingresaLogin(dni:string, id:number,nombre:string,pass:string,alias:string,tipo:string): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'ingresaLogin';
    const PARAMS = `${dni}/${id}/${nombre}/${pass}/${alias}/${tipo}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncCobrador(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobrador';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncProductor(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncProductor';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
   SyncCobradorUsu(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorUsu';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncProductorUsu(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncProductorUsu';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncAppEstadoV(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncAppEstadoV';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncCobradorOrd(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorOrd';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncCobradorPol(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorPol';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncCobradorEst(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorEst';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
   SyncCobradorTit(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorTit';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncCobradorCobCob(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorCobCob';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
   SyncCobradorCobCobOrd(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorCobCobOrd';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  SyncCobradorPgo(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorPgo';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
   SyncCobradorRen(id:number): Observable<any>{
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'SyncCobradorRen';
    const PARAMS = `${id}`;
    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
  

  validarCobrador(dni: string): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'validarCobrador';
    const PARAMS = `${dni}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }
   validarProductor(dni: string): Observable<any> {
    const SERVER = this.SERVERS.LOCAL;
    const ROUTE = 'validarProductor';
    const PARAMS = `${dni}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
  }

  // Servicios de instalacion inicial

  logIn(user: string, pass: string): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'logIn';
    const PARAMS = `${user}/${pass}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

  }

  getGpsTxt() {
    return this.http.get('gps.txt', {responseType: 'text'});
  }



  getCobradorData(id: number): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'cobradores';
    const PARAMS = `${id}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);
    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/cobradores/${id}`;
    // return this.http.get(url);

  }

  getCobranzas(idCobrador: number, licenciaCliente: string): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'cobranzas';
    const PARAMS = `${idCobrador}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/cobranzas/${idCobrador}/${licenciaCliente}`;
    // return this.http.get(url);
  }

  getOrdenesPorPoliza(idCobranza: number, licenciaCliente: string): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'ordenesPorPoliza';
    const PARAMS = `${idCobranza}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/ordenesPorPoliza/${idCobranza}/${licenciaCliente}`;
    // return this.http.get(url);
  }

  getOrdenesPolizaDetalle(idCobranza: number, idPoliza: number): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'ordenesPolizaDetalle';
    const PARAMS = `${idCobranza}/${idPoliza}/${this.licenciaCliente}/${this.loggedUserId}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/ordenesPolizaDetalle/`;
    // const params = `${idCobranza}/${idPoliza}/${this.licenciaCliente}/${this.loggedUserId}`;
    // const fullUrl = url + params;
    // return this.http.get(fullUrl);
  }

  getClientesAgenda(): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'clientesAgenda';
    const PARAMS = `${this.loggedUserId}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/clientesAgenda/${this.loggedUserId}`;
    // return this.http.get(url);
  }

  getClientesAgendaDetalle(idRegistro: number): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'clientesAgendaDetalle';
    const PARAMS = `${idRegistro}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/clientesAgendaDetalle/${idRegistro}`;
    // return this.http.get(url);
  }

  getRendicionDetalle(idRendicion: number, licenciaCliente: string): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'rendicionDetalle';
    const PARAMS = `${idRendicion}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/rendicionDetalle/${idRendicion}/${licenciaCliente}`;
    // return this.http.get(url);
  }

  getRendicionListado(idCobranza: number, licenciaCliente: string): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'rendicionListado';
    const PARAMS = `${idCobranza}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/rendicionListado/${idCobranza}/${licenciaCliente}`;
    // return this.http.get(url);
  }

  getRendicionOrdenesAcreditadas(idCobranza: number, licenciaCliente: String): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'rendicionOrdenesAcreditadas';
    const PARAMS = `${idCobranza}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/rendicionOrdenesAcreditadas/${idCobranza}/${licenciaCliente}`;
    // return this.http.get(url);
  }

  getRendicionOrdenesNoAcreditadas(idCobranza: number, licenciaCliente: String): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'rendicionOrdenesNoAcreditadas';
    const PARAMS = `${idCobranza}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/rendicionOrdenesNoAcreditadas/${idCobranza}/${licenciaCliente}`;
    // return this.http.get(url);
  }

  // PUT requests

  putAcreditarOrden(idOrdenArray: any[], idPoliza: number, latitud: number, longitud: number, licenciaCliente: String ): Observable<any> {

    const codedArray = JSON.stringify(idOrdenArray);
    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'acreditarOrden';
    const PARAMS = `${codedArray}/${idPoliza}/${latitud}/${longitud}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const codedArray = JSON.stringify(idOrdenArray);
    // const baseUrl = `https://cob.neosepelios.com.ar/cobranzasWebAPI/acreditarOrden/`;
    // const params = `${codedArray}/${idPoliza}/${latitud}/${longitud}/${licenciaCliente}`;
    // const fullUrl = baseUrl + params;
    // return this.http.get(fullUrl);
  }
   putAcreditarOrden1(idOrdenArray: any[], idPoliza: number, licenciaCliente: String ): Observable<any> {

    const codedArray = JSON.stringify(idOrdenArray);
    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'acreditarOrden1';
    const PARAMS = `${codedArray}/${idPoliza}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const codedArray = JSON.stringify(idOrdenArray);
    // const baseUrl = `https://cob.neosepelios.com.ar/cobranzasWebAPI/acreditarOrden/`;
    // const params = `${codedArray}/${idPoliza}/${latitud}/${longitud}/${licenciaCliente}`;
    // const fullUrl = baseUrl + params;
    // return this.http.get(fullUrl);
  }

  putDesacreditarOrden(idOrdenArray: any[], licenciaCliente: string): Observable<any> {

    const codedArray = JSON.stringify(idOrdenArray);
    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'desacreditarOrden';
    const PARAMS = `${codedArray}/${licenciaCliente}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const codedArray = JSON.stringify(idOrdenArray);
    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/desacreditarOrden/${codedArray}/${licenciaCliente}`;
    // return this.http.get(url);
  }

  putModificarCliente(paramsArray: any[]): Observable<any> {

    const codedArray = JSON.stringify(paramsArray);
    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'modificarCliente';
    const PARAMS = `${codedArray}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const codedArray = JSON.stringify(paramsArray);
    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/modificarCliente/${codedArray}`;
    // return this.http.get(url);
  }

  putActualizarGps(idTabla: number, idRegistro: number, latitud: string, longitud: string): Observable<any> {

    // IdTabla 1 = Agenda de Productores (idRegistro = app003_AgendaClientes.IdRegistro)
    // IdTabla 2 = Agenda de Cobradores (idRegistro = cob003_Polizas.IdPoliza)

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'actualizarGPS';
    const PARAMS = `${idTabla}/${idRegistro}/${this.licenciaCliente}/${latitud}/${longitud}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/actualizarGPS/`;
    // const params = `${idTabla}/${idRegistro}/${this.licenciaCliente}/${latitud}/${longitud}`;
    // const fullUrl = url + params;
    // return this.http.get(fullUrl);

  }

  putCobranzaModificarCliente(paramsArray: any[]): Observable<any> {

    const codedArray = JSON.stringify(paramsArray);
    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'cobranzaModificarCliente';
    const PARAMS = `${codedArray}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);


    // const codedArray = JSON.stringify(paramsArray);
    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/cobranzaModificarCliente/${codedArray}`;
    // return this.http.get(url);
  }

  putSetAltaClienteAgenda(paramsArray: any[]): Observable<any> {

    const codedArray = JSON.stringify(paramsArray);
    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'setAltaClienteAgenda';
    const PARAMS = `${codedArray}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const codedArray = JSON.stringify(paramsArray);
    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/setAltaClienteAgenda/${codedArray}`;
    // return this.http.get(url);
  }

  // POST REQUESTS

  postClienteAgenda(paramsArray: any[]): Observable<any> {

    const codedArray = JSON.stringify(paramsArray);
    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'agendarCliente';
    const PARAMS = `${codedArray}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const codedArray = JSON.stringify(paramsArray);
    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/agendarCliente/${codedArray}`;
    // return this.http.get(url);
  }

  // DELETE REQUESTS

  deleteClienteAgenda(IdRegistro: number): Observable<any> {

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'deleteClienteAgenda';
    const PARAMS = `${IdRegistro}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
    return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/deleteClienteAgenda/${IdRegistro}`;
    // return this.http.get(url);
  }


    ActualizarGpsRuta( latitud: string, longitud: string): Observable<any> {

   
    // IdTabla 1 = Agenda de Productores (idRegistro = app003_AgendaClientes.IdRegistro)
    // IdTabla 2 = Agenda de Cobradores (idRegistro = cob003_Polizas.IdPoliza)

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'actualizarGPSRuta';
    const PARAMS = `${this.licenciaCliente}/${latitud}/${longitud}`;

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE + '/' + PARAMS;
   
        
          return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/actualizarGPS/`;
    // const params = `${idTabla}/${idRegistro}/${this.licenciaCliente}/${latitud}/${longitud}`;
    // const fullUrl = url + params;
    // return this.http.get(fullUrl);

  }
  ConsultaRuta( ): Observable<any> {

   
    // IdTabla 1 = Agenda de Productores (idRegistro = app003_AgendaClientes.IdRegistro)
    // IdTabla 2 = Agenda de Cobradores (idRegistro = cob003_Polizas.IdPoliza)

    const SERVER = this.SERVERS.REMOTE;
    const ROUTE = 'ConsultaRuta';

    const url = SERVER.HOST + '/' + SERVER.BACKEND_FOLDER + '/' + ROUTE;
   
        
          return this.http.get(url);

    // const url = `https://cob.neosepelios.com.ar/cobranzasWebAPI/actualizarGPS/`;
    // const params = `${idTabla}/${idRegistro}/${this.licenciaCliente}/${latitud}/${longitud}`;
    // const fullUrl = url + params;
    // return this.http.get(fullUrl);

  }
  

}
