import {
  Component,
  Injectable,
  ViewChild,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { Observable } from 'rxjs/observable';
import { Subject } from 'rxjs/Subject';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

@Injectable()
export class MenuService {

  constructor() { }

  subject = new Subject();
  get menu(): Observable<any> {
    return this.subject.asObservable();
  }
  open() {
    this.subject.next();
  }

}
