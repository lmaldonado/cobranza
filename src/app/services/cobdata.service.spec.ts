import { TestBed, inject } from '@angular/core/testing';

import { CobdataService } from './cobdata.service';

describe('CobdataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CobdataService]
    });
  });

  it('should be created', inject([CobdataService], (service: CobdataService) => {
    expect(service).toBeTruthy();
  }));
});
