import { Router } from '@angular/router';
import {
  Component,
  OnsenModule,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from 'ngx-onsenui';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CobdataService } from '../services/cobdata.service';
import { Session } from '../interfaces/app-interfaces';
import * as ons from 'onsenui';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private cobdata: CobdataService, private router: Router) { }

  title = 'Neo Cobranzas';
  user: string;
  pass: string;
  install=false;

  userId: number;
  cobradorId: number;
  logginIn = false;
  echo: string;
  ngOnInit(){
      this.cobdata.Innstalacion().subscribe((data)=>{
        console.log(data[0].contador); 
        if(data[0].contador==0){
          this.install = true;
        }else
          this.install = false;          
      


      });
   
  }

  logIn() {
    
    this.cobdata.licencia( 'vacio').subscribe((data) => {     

       if (data.status=='Active'){
         if (!this.user || !this.pass) {
            ons.notification.alert('Debe ingresar un usuario y una contraseña.');
          } else {
            this.logginIn = true;
            this.cobdata.logIn(this.user, this.pass).subscribe( (data) => {
              if (data[0].error) {
                ons.notification.alert(data[0].error);
                this.logginIn = false;
              } else {
                const msg = `Desea Sincronizar la data`;
                const confirmMsg = `Sr Sincronizo con exito!`;
                ons.notification.confirm(msg, {title: '', buttonLabels: ['No', 'Si']})
                .then((res) => {
                  if (res) {
                    this.router.navigate(['Sync']);
                  } else {
                  }
                });
                if (data[0].Tipo === 'cobrador') {

                  this.cobdata.tipoUsuario = data[0].Tipo;
                  this.cobdata.loggedUserId = data[0].id;
                  this.cobdata.cobradorId = data[0].IdTipoUsuario;
                  this.cobdata.licenciaCliente = data[0].LicenciaCliente;
                  this.cobdata.usuarioNombre = data[0].NombreUsuario;
                  this.cobdata.usuarioNombre = data[0].NombreUsuario;
                  this.cobdata.usuarioDocumento = data[0].Documento;
                  this.cobdata.cobradorComision = data[0].Comision;
                  this.router.navigate(['mainc']);

                } else if (data[0].Tipo === 'productor') {

                  this.cobdata.tipoUsuario = data[0].Tipo;
                  this.cobdata.loggedUserId = data[0].id;
                  this.cobdata.productorId = data[0].IdTipoUsuario;
                  this.cobdata.licenciaCliente = data[0].LicenciaCliente;
                  this.cobdata.usuarioNombre = data[0].NombreUsuario;
                  this.cobdata.usuarioDocumento = data[0].Documento;
                  this.router.navigate(['mainp']);

                } else {

                  this.logginIn = false;
                  this.router.navigate(['']);

                }

              }
            });
          }

       }else{
         ons.notification.alert('la Licencia esta inactiva');
       }
      });

   
    

  }

  installWizard(){
    this.router.navigate(['step-01']);
  }
  
  

}
